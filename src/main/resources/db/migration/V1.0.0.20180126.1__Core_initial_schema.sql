CREATE TABLE _address (
  uid bigserial NOT NULL,
  uuid char(36),
  city varchar(255),
  country_code varchar(255),
  postal_code varchar(255),
  province varchar(255),
  street_address varchar(255),
  street_address_details varchar(255),
  PRIMARY KEY (uid)
);

CREATE TABLE _phone (
  uid bigserial NOT NULL,
  uuid char(36),
  serial_number varchar(255),
  measurement_information_uid int8,
  owner_uid int8,
  phone_type_uid int8,
  phone_size_uid int8,
  PRIMARY KEY (uid)
);

CREATE TABLE _phone_type (
  uid bigserial NOT NULL,
  uuid char(36),
  phone_type_code char(2),
  description varchar(255),
  PRIMARY KEY (uid)
);

CREATE TABLE _currency (
  uid bigserial NOT NULL,
  uuid char(36),
  iso_code varchar(255),
  iso_symbol varchar(255),
  name varchar(255),
  PRIMARY KEY (uid)
);

CREATE TABLE _measurement (
  uid bigserial NOT NULL,
  uuid char(36),
  measurement_value float8,
  unit_of_measure_uid int8,
  PRIMARY KEY (uid)
);

CREATE TABLE _measurement_information (
  uid bigserial NOT NULL,
  uuid char(36),
  exterior_height_uid int8,
  exterior_length_uid int8,
  exterior_width_uid int8,
  volume_uid int8,
  weight_uid int8,
  PRIMARY KEY (uid)
);

CREATE TABLE _organization (
  uid bigserial NOT NULL,
  uuid char(36),
  name varchar(255),
  address_uid int8,
  PRIMARY KEY (uid)
);

CREATE TABLE _organization_user_registrations (
  user_registration int8 NOT NULL,
  organization int8 NOT NULL
);

CREATE TABLE _permission (
  uid bigserial NOT NULL,
  uuid char(36),
  name varchar(255),
  PRIMARY KEY (uid)
);

CREATE TABLE _role (
  uid bigserial NOT NULL,
  uuid char(36),
  name varchar(255),
  PRIMARY KEY (uid)
);

CREATE TABLE _role_permissions (
  role_id int8 NOT NULL,
  permission_id int8 NOT NULL
);

CREATE TABLE _secret_question (
  uid bigserial NOT NULL,
  uuid char(36),
  question varchar(255),
  PRIMARY KEY (uid)
);

CREATE TABLE _security_level_configuration (
  uid bigserial NOT NULL,
  uuid char(36),
  allow_mixed_case boolean,
  check_minimum_length boolean,
  check_valid_period boolean,
  email_as_user_name boolean,
  exclude_repeating_characters boolean,
  exclude_username_email boolean,
  include_numbers_and_letters boolean,
  include_special_characters boolean,
  minimum_password_length int4,
  registration_verification_required boolean,
  valid_period int4,
  PRIMARY KEY (uid)
);

CREATE TABLE _unit_of_meausre (
  uid bigserial NOT NULL,
  uuid char(36),
  iso_code varchar(255),
  iso_symbol varchar(255),
  name varchar(255),
  PRIMARY KEY (uid)
);

CREATE TABLE _user (
  uid bigserial NOT NULL,
  uuid char(36),
  active_indicator boolean,
  email varchar(255),
  first_name varchar(255),
  last_name varchar(255),
  middle_initial varchar(255),
  password varchar(255),
  secret_question_answer varchar(255),
  username varchar(255),
  secret_question_uid int8,
  PRIMARY KEY (uid)
);

CREATE TABLE _user_roles (
  user_id int8 NOT NULL,
  role_id int8 NOT NULL
);