package com.samsunginc.service;

import com.samsunginc.model.SecretQuestion;
import com.samsunginc.repository.SecretQuestionRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for the {@link SecretQuestion} entities related operations.
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
@Service
public class SecretQuestionService {

    @Autowired
    private SecretQuestionRepository secretQuestionRepository;

    /**
     * Returns all the {@link SecretQuestion}
     * @return list of {@link SecretQuestion}
     */
    public List<SecretQuestion> list() {
        return secretQuestionRepository.findAll();
    }

    /**
     * Return a {@link SecretQuestion} by its identifier
     * @param uid the {@link SecretQuestion} identifier
     * @return the {@link SecretQuestion} with the given {@code uid}
     */
    public SecretQuestion findOne(Long uid) {
        return secretQuestionRepository.findOne(uid);
    }
}
