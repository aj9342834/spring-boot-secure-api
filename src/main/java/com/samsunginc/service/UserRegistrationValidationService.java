package com.samsunginc.service;

import com.samsunginc.model.SecurityLevelConfiguration;
import com.samsunginc.model.User;
import com.samsunginc.repository.UserRepository;
import com.fasterxml.jackson.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Service related to {@link User} registration validation operations
 *
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
@Service
public class UserRegistrationValidationService {

    protected static final String USERNAME = "username";
    protected static final String USERNAME_REGEXP = "^[A-Za-z0-9_$]+$";
    protected static final String EMAIL = "email";
    protected static final String EMAIL_REGEXP = "^[A-Za-z0-9_\\-.$]+@[A-Za-z0-9_]+(\\.[A-Za-z0-9_]+)*$";
    protected static final String SPECIAL_CHARACTERS_REGEXP = "^.*[!\"#$%&\'()*+,-./:;<=>?@\\[\\]\\^_`{|\\}~].*$";
    protected static final String ALPHANUMERIC_REGEXP = "^.*([\\d].*[a-zA-Z]|[a-zA-Z].*[\\d]).*$";
    protected static final String PASSWORD = "password";

    protected static final String NOT_UNIQUE = "NotUnique";
    protected static final String NOT_UNIQUE_EMAIL = "NotUniqueEmail";
    protected static final String INVALID = "Invalid";
    protected static final String INVALID_EMAIL = "InvalidEmail";
    protected static final String REQUIRED = "Required";
    protected static final String MINIMUM = "Minimum";
    protected static final String SPECIAL_CHARACTERS = "SpecialCharacters";
    protected static final String ALPHANUMERIC = "Alphanumeric";
    protected static final String REPEATING_CHARACTERS = "RepeatingCharacters";
    protected static final String NOT_USER_NAME_OR_EMAIL = "NotUserNameOrEmail";
    protected static final String EXCEED_MAX_LENGTH = "ExceedMaxLength";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SecurityLevelConfigurationService securityLevelConfigurationService;

    /**
     * Validates that the passed first name attribute is a valid one and it is
     * not already assigned to an {@link User}
     *
     * @param user the {@link User} with the target {@code firstName} to validate
     * @return a {@link HasFieldError} object containing the errors found.<br>
     * If the {@link HasFieldError}'s {@code fieldErrors} list is empty
     * then the passed value is a valid first name to be used
     */
    public HasFieldError validateFirstName(@NotNull User user) {
        List<String> fieldErrors = validateName(user.getFirstName());
        return new FieldError().setField("firstName").setErrors(fieldErrors);
    }

    private List<String> validateName(@NotNull String name) {
        List<String> fieldErrors = new ArrayList<>();
        if (StringUtils.isEmpty(name)) {
            fieldErrors.add(REQUIRED);
        } else if (name.length() > 60) {
            fieldErrors.add(EXCEED_MAX_LENGTH);
        }
        return fieldErrors;
    }

    /**
     * Validates that the passed last name attribute is a valid one and it is
     * not already assigned to an {@link User}
     *
     * @param user the {@link User} with the target {@code lastName} to validate
     * @return a {@link HasFieldError} object containing the errors found.<br>
     * If the {@link HasFieldError}'s {@code fieldErrors} list is empty
     * then the passed value is a valid first last to be used
     */
    public HasFieldError validateLastName(User user) {
        List<String> fieldErrors = validateName(user.getLastName());
        return new FieldError().setField("lastName").setErrors(fieldErrors);
    }

    /**
     * Validates that the passed username attribute is a valid one and it is
     * not already assigned to an {@link User}
     *
     * @param user the {@link User} with the target {@code username} to validate
     * @return a {@link HasFieldError} object containing the errors found.<br>
     * If the {@link HasFieldError}'s {@code fieldErrors} list is empty
     * then the passed value is a valid username to be used
     */
    public HasFieldError validateUsername(@NotNull User user) {
        List<String> fieldErrors = new ArrayList<>();
        if (StringUtils.isEmpty(user.getUsername())) {
            fieldErrors.add(REQUIRED);
        } else if (securityLevelConfigurationService.getSecurityConfiguration().getEmailAsUserName()) {
            // If the configuration is set to use email as username then only perform email validations
            if (!isValidEmail(user.getUsername())) {
                fieldErrors.add(INVALID_EMAIL);
            } else if (userRepository.countByEmail(user.getUsername()) > 0) {
                fieldErrors.add(NOT_UNIQUE_EMAIL);
            }
        } else if (!isValidUsername(user.getUsername()) && !isValidEmail(user.getUsername())) {
            fieldErrors.add(INVALID);
        } else if (isValidUsername(user.getUsername()) && userRepository.countByUsername(user.getUsername()) > 0
                || isValidEmail(user.getUsername()) && userRepository.countByEmail(user.getUsername()) > 0) {
            // The user can use its email as username, thus there should not be another user with the given email
            fieldErrors.add(NOT_UNIQUE);
        }
        return new FieldError().setField(USERNAME).setErrors(fieldErrors);
    }

    /**
     * Returns weather the user has a valid username
     *
     * @param username a {@link NotNull} username to be verified
     * @return {@code true} if is is a valid username, {@code false} otherwise
     */
    protected final boolean isValidUsername(@NotNull String username) {
        return Pattern.compile(USERNAME_REGEXP).matcher(username).matches();
    }

    /**
     * Validates that the passed email attribute is a valid email and it is
     * not already assigned to an {@link User}
     *
     * @param user the {@link User} with the target {@code email} to validate
     * @return a {@link HasFieldError} object containing the errors found.<br>
     * If the {@link HasFieldError}'s {@code fieldErrors} list is empty
     * then the passed value is a valid email to be used
     */
    public HasFieldError validateUserEmail(@NotNull User user) {
        List<String> fieldErrors = new ArrayList<>();
        if (StringUtils.isEmpty(user.getEmail())) {
            fieldErrors.add(REQUIRED);
        } else if (!isValidEmail(user.getEmail())) {
            fieldErrors.add(INVALID);
        } else if (userRepository.countByEmail(user.getEmail()) > 0) {
            fieldErrors.add(NOT_UNIQUE);
        }
        return new FieldError().setField(EMAIL).setErrors(fieldErrors);
    }

    /**
     * Return weather the passed value is a valid email address
     *
     * @param email a {@link NotNull} email address value to be verified
     * @return {@code true} if it is a valid email address, {@code false} otherwise
     */
    protected final boolean isValidEmail(@NotNull String email) {
        return Pattern.compile(EMAIL_REGEXP).matcher(email).matches();
    }

    /**
     * Validates that the passed password attribute fulfill the restrictions found in the
     * current application's {@link SecurityLevelConfiguration}
     *
     * @param user the {@link User} with the password to be verified
     * @return a {@link HasFieldError} object containing the errors found.<br>
     * If the {@link HasFieldError}'s{@code fieldErrors} list is empty
     * then the passed value is a valid email to be used
     */
    public HasFieldError validateUserPassword(@NotNull User user) {
        SecurityLevelConfiguration securityConfiguration = securityLevelConfigurationService.getSecurityConfiguration();
        List<String> errors = new ArrayList<>();
        if (StringUtils.isEmpty(user.getPassword())) {
            errors.add(REQUIRED);
        } else {
            for (PasswordValidationType validationType : PasswordValidationType.values()) {
                PasswordValidationOperation validationOperation = PasswordValidationOperationFactory.buildValidationOperation(validationType, securityConfiguration);
                if (!validationOperation.hasValidPassword(user)) {
                    errors.add(validationOperation.getValidationMessage());
                }
            }
        }
        return new FieldError().setField(PASSWORD).setErrors(errors);
    }

    protected enum PasswordValidationType {
        MINIMUM_LENGTH, INCLUDE_SPECIAL_CHARS, ALPHANUMERIC, EXCLUDE_REPEATING_CHARS, EXCLUDE_USER_AND_EMAIL
    }

    protected static final class PasswordValidationOperationFactory {
        static PasswordValidationOperation buildValidationOperation(PasswordValidationType validationType, SecurityLevelConfiguration configuration) {
            switch (validationType) {
                case MINIMUM_LENGTH:
                    return new MinimumLengthPasswordValidation(configuration);
                case INCLUDE_SPECIAL_CHARS:
                    return new IncludeSpecialCharacterPasswordValidation(configuration);
                case ALPHANUMERIC:
                    return new AlphanumericPasswordValidation(configuration);
                case EXCLUDE_REPEATING_CHARS:
                    return new ExcludeRepeatingCharactersPasswordValidation(configuration);
                case EXCLUDE_USER_AND_EMAIL:
                    return new ExcludeEmailAndUsernamePasswordValidation(configuration);
            }
            return new AlwaysTruePasswordValidation(configuration);
        }
    }

    protected static abstract class PasswordValidationOperation {
        protected final SecurityLevelConfiguration configuration;

        protected PasswordValidationOperation(SecurityLevelConfiguration configuration) {
            this.configuration = configuration;
        }

        abstract boolean hasValidPassword(User user);

        abstract String getValidationMessage();
    }

    protected static final class MinimumLengthPasswordValidation extends PasswordValidationOperation {

        protected MinimumLengthPasswordValidation(SecurityLevelConfiguration configuration) {
            super(configuration);
        }

        @Override
        public boolean hasValidPassword(User user) {
            if (configuration.getCheckMinimumLength()) {
                return configuration.getMinimumPasswordLength() <= user.getPassword().length();
            }
            return true;
        }

        @Override
        public String getValidationMessage() {
            return MINIMUM;
        }
    }

    protected static final class IncludeSpecialCharacterPasswordValidation extends PasswordValidationOperation {

        protected IncludeSpecialCharacterPasswordValidation(SecurityLevelConfiguration configuration) {
            super(configuration);
        }

        @Override
        boolean hasValidPassword(User user) {
            if (configuration.getIncludeSpecialCharacters()) {
                return Pattern.compile(SPECIAL_CHARACTERS_REGEXP).matcher(user.getPassword()).matches();
            }
            return true;
        }

        @Override
        String getValidationMessage() {
            return SPECIAL_CHARACTERS;
        }
    }

    protected static final class AlphanumericPasswordValidation extends PasswordValidationOperation {

        protected AlphanumericPasswordValidation(SecurityLevelConfiguration configuration) {
            super(configuration);
        }

        @Override
        boolean hasValidPassword(User user) {
            if (configuration.getIncludeNumbersAndLetters()) {
                return Pattern.compile(ALPHANUMERIC_REGEXP).matcher(user.getPassword()).matches();
            }
            return true;
        }

        @Override
        String getValidationMessage() {
            return ALPHANUMERIC;
        }
    }

    protected static final class ExcludeRepeatingCharactersPasswordValidation extends PasswordValidationOperation {

        protected ExcludeRepeatingCharactersPasswordValidation(SecurityLevelConfiguration configuration) {
            super(configuration);
        }

        @Override
        boolean hasValidPassword(User user) {
            if (configuration.getExcludeRepeatingCharacters()) {
                String password = user.getPassword();
                for (int i = 0; i < password.length() - 1; i++) {
                    String charAtI = checkForAllowedMixedConfiguration(String.valueOf(password.charAt(i)));
                    String charAtJ = checkForAllowedMixedConfiguration(String.valueOf(password.charAt(i+1)));
                    if (charAtI.equals(charAtJ)) {
                        return false;
                    }
                }
            }
            return true;
        }

        private String checkForAllowedMixedConfiguration(@NotNull String target) {
            return configuration.getAllowMixedCase() ? target.toLowerCase() : target;
        }

        @Override
        String getValidationMessage() {
            return REPEATING_CHARACTERS;
        }
    }

    protected static final class ExcludeEmailAndUsernamePasswordValidation extends PasswordValidationOperation {

        protected ExcludeEmailAndUsernamePasswordValidation(SecurityLevelConfiguration configuration) {
            super(configuration);
        }

        @Override
        protected boolean hasValidPassword(User user) {
            if (configuration.getExcludeUserNameAndEmail()) {
                return !user.getPassword().contains(user.getUsername()) && !user.getPassword().contains(user.getEmail());
            }
            return true;
        }

        @Override
        protected String getValidationMessage() {
            return NOT_USER_NAME_OR_EMAIL;
        }
    }

    private static class AlwaysTruePasswordValidation extends PasswordValidationOperation {
        public AlwaysTruePasswordValidation(SecurityLevelConfiguration configuration) {
            super(configuration);
        }

        @Override
        boolean hasValidPassword(User user) {
            return true;
        }

        @Override
        String getValidationMessage() {
            return null;
        }
    }




    /**
     * Represents the Google's response when quering the REST API for the reCaptcha verification
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonPropertyOrder({
            "success",
            "challenge_ts",
            "hostname",
            "error-codes"
    })
    public static class GoogleResponse {

        @JsonProperty("success")
        private boolean success;

        @JsonProperty("challenge_ts")
        private String challengeTs;

        @JsonProperty("hostname")
        private String hostname;

        @JsonProperty("error-codes")
        private ErrorCode[] errorCodes;

        @JsonIgnore
        public boolean hasClientError() {
            ErrorCode[] errors = getErrorCodes();
            if(errors == null) {
                return false;
            }
            for(ErrorCode error : errors) {
                switch(error) {
                    case InvalidResponse:
                    case MissingResponse:
                        return true;
                }
            }
            return false;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getChallengeTs() {
            return challengeTs;
        }

        public void setChallengeTs(String challengeTs) {
            this.challengeTs = challengeTs;
        }

        public String getHostname() {
            return hostname;
        }

        public void setHostname(String hostname) {
            this.hostname = hostname;
        }

        public ErrorCode[] getErrorCodes() {
            return errorCodes;
        }

        public void setErrorCodes(ErrorCode[] errorCodes) {
            this.errorCodes = errorCodes;
        }
    }

    enum ErrorCode {
        MissingSecret,     InvalidSecret,
        MissingResponse,   InvalidResponse;

        private static Map<String, ErrorCode> errorsMap = new HashMap<String, ErrorCode>(4);

        static {
            errorsMap.put("missing-input-secret",   MissingSecret);
            errorsMap.put("invalid-input-secret",   InvalidSecret);
            errorsMap.put("missing-input-response", MissingResponse);
            errorsMap.put("invalid-input-response", InvalidResponse);
        }

        @JsonCreator
        public static ErrorCode forValue(String value) {
            return errorsMap.get(value.toLowerCase());
        }
    }

    /**
     * Has the validation errors found at a given {@link User} field
     */
    public interface HasFieldError {
        /**
         * Field target being validated
         */
        String getField();

        /**
         * Validation errors found at {@code field}
         */
        List<String> getErrors();
    }

    private class FieldError implements HasFieldError{

        private String field;

        private List<String> errors;

        public String getField() {
            return field;
        }

        private FieldError setField(String field) {
            this.field = field;
            return this;
        }

        public List<String> getErrors() {
            return errors;
        }

        private FieldError setErrors(List<String> errors) {
            this.errors = errors;
            return this;
        }
    }

    public static class ReCapthcaException extends RuntimeException {

        public ReCapthcaException(String s) {
            super(s);
        }
    }

    public static class InvalidReCaptchaException extends ReCapthcaException {
        public InvalidReCaptchaException(String s) {
            super(s);
        }
    }

    public static class ReCaptchaInvalidException extends ReCapthcaException {
        public ReCaptchaInvalidException(String s) {
            super(s);
        }
    }
}
