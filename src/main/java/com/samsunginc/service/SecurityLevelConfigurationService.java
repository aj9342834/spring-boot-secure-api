package com.samsunginc.service;

import com.samsunginc.model.SecurityLevelConfiguration;
import com.samsunginc.repository.SecurityLevelConfigurationRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service for retrieval and update of the system's Security Configuration.
 *
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
@Service
public class SecurityLevelConfigurationService {

    @Autowired
    private SecurityLevelConfigurationRepository securityLevelConfigurationRepository;


    /**
     * Get the current system's Security Configuration .<br>
     * If a Security Configuration is not found then the method create it.
     * @return the system's Security Configuration object
     */
    public SecurityLevelConfiguration getSecurityConfiguration() {
        SecurityLevelConfiguration securityLevelConfiguration = createSecurityConfigurationIfNotFound();
        return securityLevelConfiguration;
    }

    private SecurityLevelConfiguration createSecurityConfigurationIfNotFound() {
        SecurityLevelConfiguration securityLevelConfiguration = securityLevelConfigurationRepository.findOne(1L);
        if (securityLevelConfiguration == null)
            securityLevelConfiguration = securityLevelConfigurationRepository.save(new SecurityLevelConfiguration());
        return securityLevelConfiguration;
    }

    /**
     * Updates the system's Security Configuration
     * @param securityLevelConfiguration
     * @return the updated system's Security Configuration object
     */
    public SecurityLevelConfiguration updateSecurityConfiguration(SecurityLevelConfiguration securityLevelConfiguration) {
        SecurityLevelConfiguration targetSecurityLevelConfiguration = getSecurityConfiguration();
        copySecurityConfigurationProperties(securityLevelConfiguration, targetSecurityLevelConfiguration);
        return securityLevelConfigurationRepository.save(targetSecurityLevelConfiguration);
    }

    private void copySecurityConfigurationProperties(SecurityLevelConfiguration source, SecurityLevelConfiguration target) {
        BeanUtils.copyProperties(source, target, new String[] {"uid"});
    }
}
