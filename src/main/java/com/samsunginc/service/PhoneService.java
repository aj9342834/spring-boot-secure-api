package com.samsunginc.service;

import com.samsunginc.model.Phone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import org.springframework.beans.BeanUtils;
import com.samsunginc.repository.PhoneRepository;
import java.util.Optional;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Service
public class PhoneService {
    @Autowired
    private PhoneRepository containerRepository;

    public Optional<Phone> findById(Long uid) {
        return containerRepository.findByUid(uid);
    }
    
    public List<Phone> list() {

        List<Phone> containers = containerRepository.findAll();

        return containers;
    }
    
    public Optional<Phone> add(Phone phone) {

        Optional<Phone> savedContainer = Optional.of(containerRepository.saveAndFlush(phone));

        return savedContainer;
    }

    public Optional<Phone> update(Long id, Phone phone) {
        Optional<Phone> existingPhone = findById(id);
        
        if (!existingPhone.isPresent()) {
            return null;
        }
        
        BeanUtils.copyProperties(phone, existingPhone.get(), "uid", "uuid");
        Optional<Phone> updatedPhone = Optional.of(containerRepository.saveAndFlush(existingPhone.get()));

        return updatedPhone;
    }

    public void deleteMultiple(List<Phone> phones) {
        containerRepository.delete(phones);
    }

    public void delete(Long id) {
        containerRepository.delete(id);
    }

}
