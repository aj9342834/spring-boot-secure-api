package com.samsunginc.service;

import java.time.LocalDateTime;
import java.util.List;

import com.samsunginc.model.UserStatus;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.samsunginc.model.User;
import com.samsunginc.repository.UserRepository;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserNotificationService userNotificationService;

    public List<User> list() {

        List<User> users = userRepository.findAll();

        return users;
    }

    public User findOne(Long id) {
        return userRepository.findOne(id);
    }
    
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public List<User> addMultiple(List<User> users) {

        List<User> savedUsers = userRepository.save(users);

        return savedUsers;
    }

    public User add(User user) {
        user.setRegistrationDateTime(LocalDateTime.now());
        User savedUser = userRepository.saveAndFlush(user);

        return savedUser;
    }

    public User update(Long id, User user) {

        User exisitingUser = userRepository.findOne(id);
        
        if (exisitingUser == null) {
            return null;
        }
        
        BeanUtils.copyProperties(user, exisitingUser, "uid", "uuid");
        User updatedUser = userRepository.saveAndFlush(exisitingUser);

        return updatedUser;
    }

    public void deleteMultiple(List<User> users) {
        userRepository.delete(users);
    }

    public void delete(Long id) {
        userRepository.delete(id);
    }

    /**
     * Toggles the {@link User} {@code userStatus} property.<br/>
     * If the {@link User}'s status is {@code ACTIVE} then it is set to {@code INACTIVE} and viceversa
     * @param id {@link User} identifier
     * @return the updated {@link User} with the new status
     */
    public User toggleUserStatus(Long id) {
        User user = this.findOne(id);
        if (user != null) {
            switch(user.getUserStatus()) {
                case ACTIVE:
                    return updateUserStatus(user, UserStatus.INACTIVE);
                case PENDING:
                case INACTIVE:
                    return updateUserStatus(user, UserStatus.ACTIVE);
            }
        }
        return user;
    }

    /**
     * Activates the {@link User} with the corresponding {@code id} and sent mail notifications
     * @param id {@link User} identifier
     * @return the updated {@link User} with the new status
     */
    public User activateUser(Long id) {
        return this.updateUserStatus(id, UserStatus.ACTIVE);
    }

    /**
     * Reject the {@link User} with the corresponding {@code id} and sent mail notifications
     * @param id {@link User} identifier
     */
    public void rejectUser(Long id) {
        this.updateUserStatus(id, UserStatus.REJECTED);
    }

    /**
     * Deactivates the {@link User} with the corresponding {@code id} and sent mail notifications
     * @param id {@link User} identifier
     * @return the updated {@link User} with the new status
     */
    public User deactivateUser(Long id) {
        return this.updateUserStatus(id, UserStatus.INACTIVE);
    }

    /**
     * Updates the {@link User} with the corresponding {@code id} and sent mail notifications
     * @param id {@link User} identifier
     * @param status the new {@link User} status
     * @return the updated {@link User} with the new status
     *          or {@code null} if the {@link User} is rejected
     */
    public User updateUserStatus(Long id, UserStatus status) {
        User user = userRepository.getOne(id);
        return updateUserStatus(user, status);
    }

    public User updateUserStatus(User user, UserStatus status) {
        switch (status) {
            case REJECTED:
                userRepository.delete(user);
                if (user != null) {
                    userNotificationService.sendUserRejectionNotification(user, "Access Denied");
                    user = null;
                }
                break;
            case ACTIVE:
            case INACTIVE:
                if (user != null) {
                    user.setUserStatus(status);
                    user = userRepository.save(user);
                    if (UserStatus.ACTIVE == status) {
                        userNotificationService.sendUserActivationNotification(user);
                        userNotificationService.sendMailSentNotificationToAdministrators(user);
                    }
                }
        }
        return user;
    }
}
