package com.samsunginc.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.samsunginc.model.Measurement;
import com.samsunginc.repository.MeasurementRepository;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Service
public class MeasurementService {

    @Autowired
    MeasurementRepository measurementRepository;

    public List<Measurement> list() {

        List<Measurement> measurements = measurementRepository.findAll();

        return measurements;
    }

    public Measurement findOne(Long id) {
        return measurementRepository.findOne(id);
    }

    public List<Measurement> addMultiple(List<Measurement> measurements) {

        List<Measurement> savedMeasurements = measurementRepository.save(measurements);

        return savedMeasurements;
    }

    public Measurement add(Measurement measurement) {

        Measurement savedMeasurement = measurementRepository.saveAndFlush(measurement);

        return savedMeasurement;
    }

    public Measurement update(Long id, Measurement measurement) {

        Measurement exisitingMeasurement = measurementRepository.findOne(id);
        
        if (exisitingMeasurement == null) {
            return null;
        }
        
        BeanUtils.copyProperties(measurement, exisitingMeasurement, "uid", "uuid");
        Measurement updatedMeasurement = measurementRepository.saveAndFlush(exisitingMeasurement);

        return updatedMeasurement;
    }

    public void deleteMultiple(List<Measurement> measurements) {
        measurementRepository.delete(measurements);
    }

    public void delete(Long id) {
        measurementRepository.delete(id);
    }

}
