package com.samsunginc.service.etl;
/**
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
public class DataConversionException extends Exception {

    public DataConversionException() {
        super();
    }

    public DataConversionException(String message) {
        super(message);
    }

    public DataConversionException(Throwable cause) {
        super(cause);
    }

    public DataConversionException(String message, Throwable cause) {
        super(message, cause);
    }
}
