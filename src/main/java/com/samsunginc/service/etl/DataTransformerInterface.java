package com.samsunginc.service.etl;
/**
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
public interface DataTransformerInterface<IN, OUT> {
	public OUT transform(IN data) throws DataConversionException;
}