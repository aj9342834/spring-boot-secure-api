package com.samsunginc.service.etl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.StringWriter;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.milyn.Smooks;
import org.milyn.SmooksException;
import org.milyn.smooks.edi.unedifact.UNEdifactReaderConfigurator;
/**
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
public class EdifactToJavaTransformer implements DataTransformerInterface<FileInputStream, String> {

    private String mappingModel;

    public EdifactToJavaTransformer(String mappingModel) {
        this.mappingModel = mappingModel;
    }

    public String getMappingModel() {
        return mappingModel;
    }

    public EdifactToJavaTransformer setMappingModel(String mappingModel) {
        this.mappingModel = mappingModel;
        return this;
    }

    @Override
    public String transform(FileInputStream file) throws DataConversionException {
        Smooks smooks = new Smooks();

        if (this.mappingModel == null) {
            throw new DataConversionException("Mapping model cannot be null");
        }

        smooks.setReaderConfig(new UNEdifactReaderConfigurator(this.mappingModel));

        try {
            StringWriter writer = new StringWriter();

            smooks.filterSource(new StreamSource(file), new StreamResult(writer));

            return writer.toString();
        } catch (SmooksException e) {
            throw new DataConversionException(e);
        } finally {
            smooks.close();
        }
    }

}
