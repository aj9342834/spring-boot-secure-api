package com.samsunginc.service;

import com.samsunginc.model.User;
import com.samsunginc.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.thymeleaf.TemplateEngine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service to send mail notification for {@link User} related operations
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
@Service
@Qualifier("userRegistrationNotificationService")
public class UserNotificationService extends AbstractMailNotificationService<AbstractMailNotificationService.MultipartMailNotificationConfiguration> {

    @Value("${samsung.general.UI.uri}")
    private String userInterfaceLocation;

    @Value("${samsung.general.UI.uri.userAdministration.tag}")
    private String userAdministrationTag;

    @Value("${samsung.general.UI.uri.login.tag}")
    private String userLoginTag;

    @Autowired
    protected UserNotificationService(UserRepository userRepository, JavaMailSender javaMailSender, MessageSource messageSource, TemplateEngine templateEngine) {
        super(javaMailSender, messageSource, templateEngine, userRepository);
    }

    /**
     * Sends a mail to the Systems' administrator that a new user request was made.
     * @param newUser with the data from which the mail body is composed
     */
    public void sendNewUserRequestNotification(User newUser) {
        String messageSubject = getMessage("mail.notification.newUserRequest.subject");
        Map<String, String> bodyParams = new HashMap<>();
        bodyParams.put("userCompleteName", getUserCompleteName(newUser));
        bodyParams.put("userManagerLink", userInterfaceLocation + "#" + userAdministrationTag);
        String[] administrators = getSystemAdministratorsEmails();
        sendNotification(newUser, administrators, messageSubject, bodyParams, "mail/text/newUserRequest", "mail/html/newUserRequest");
    }

    private String getUserCompleteName(User newUser) {
        String firstName = newUser.getFirstName();
        String lastName = newUser.getLastName();
        firstName = StringUtils.isEmpty(firstName) ? "" : firstName;
        lastName = StringUtils.isEmpty(lastName) ? "" : lastName;
        String completeName = StringUtils.isEmpty(firstName + lastName) ? newUser.getEmail() : firstName + " " + lastName;
        return completeName;
    }

    private void sendNotification(User user, String[] to, String messageSubject, Map<String, String> bodyParams, String plainTextTemplate) {
        String messageBody = buildMessageBodyFromTemplate(bodyParams, plainTextTemplate);
        sendNotification(() -> {
            MultipartMailNotificationConfiguration configuration = new MultipartMailNotificationConfiguration(to,
                    getSystemNoReplySenderName(), getSystemNoReplySenderAddress(), messageSubject, messageBody, messageBody, getDefaultEncoding(), user);
            return configuration;
        });
    }

    private void sendNotification(User user, String[] to, String messageSubject, Map<String, String> bodyParams, String plainTextMailTemplate, String multiPartMailTemplate) {
        String plainTextMailBody = buildMessageBodyFromTemplate(bodyParams, plainTextMailTemplate);
        String multipartMailBody = buildMessageBodyFromTemplate(bodyParams, multiPartMailTemplate);
        sendNotification(() -> {
            MultipartMailNotificationConfiguration configuration = new MultipartMailNotificationConfiguration(to,
                    getSystemNoReplySenderName(), getSystemNoReplySenderAddress(), messageSubject, plainTextMailBody, multipartMailBody, getDefaultEncoding(), user);
            return configuration;
        });
    }



    public void sendUserActivationNotification(User user) {
        String messageSubject = getMessage("mail.notification.userActivated.subject");
        Map<String, String> bodyParams = new HashMap<>();
        bodyParams.put("userCompleteName", getUserCompleteName(user));
        bodyParams.put("userLoginLink", userInterfaceLocation + "#" + userLoginTag);
        sendNotification(user, new String[] {user.getEmail()}, messageSubject, bodyParams, "mail/text/userAccountActivated", "mail/html/userAccountActivated");
    }

    public void sendUserRejectionNotification(User user, String rejectionReason) {
        String messageSubject = getMessage("mail.notification.userRejected.subject");
        Map<String, String> bodyParams = new HashMap<>();
        bodyParams.put("userCompleteName", getUserCompleteName(user));
        bodyParams.put("userLoginLink", userInterfaceLocation + "#" + userLoginTag);
        bodyParams.put("rejectReason", rejectionReason);
        sendNotification(user, new String[] {user.getEmail()}, messageSubject, bodyParams, "mail/text/userAccountRequestRejected", "mail/html/userAccountRequestRejected");
    }

    public void sendMailSentNotificationToAdministrators(User user) {
        String messageSubject = getMessage("mail.notification.userAccountNotificationSent.subject");
        Map<String, String> bodyParams = new HashMap<>();
        bodyParams.put("userCompleteName", getUserCompleteName(user));
        String[] administrators = getSystemAdministratorsEmails();
        sendNotification(user, administrators, messageSubject, bodyParams, "mail/text/userAccountNotificationSent", "mail/html/userAccountNotificationSent");
    }
}
