package com.samsunginc.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.samsunginc.model.UnitOfMeasure;
import com.samsunginc.repository.UnitOfMeasureRepository;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Service
public class UnitOfMeasureService {

    @Autowired
    UnitOfMeasureRepository unitOfMeasureRepository;

    public List<UnitOfMeasure> list() {

        List<UnitOfMeasure> unitsOfMeasure = unitOfMeasureRepository.findAll();

        return unitsOfMeasure;
    }

    public UnitOfMeasure findOne(Long id) {
        return unitOfMeasureRepository.findOne(id);
    }

    public List<UnitOfMeasure> addMultiple(List<UnitOfMeasure> unitsOfMeasure) {

        List<UnitOfMeasure> savedUnitsOfMeasure = unitOfMeasureRepository.save(unitsOfMeasure);

        return savedUnitsOfMeasure;
    }

    public UnitOfMeasure add(UnitOfMeasure unitOfMeasure) {

        UnitOfMeasure savedUnitOfMeasure = unitOfMeasureRepository.saveAndFlush(unitOfMeasure);

        return savedUnitOfMeasure;
    }

    public UnitOfMeasure update(Long id, UnitOfMeasure unitOfMeasure) {

        UnitOfMeasure exisitingUnitOfMeasure = unitOfMeasureRepository.findOne(id);
        
        if (exisitingUnitOfMeasure == null) {
            return null;
        }
        
        BeanUtils.copyProperties(unitOfMeasure, exisitingUnitOfMeasure, "uid", "uuid");
        UnitOfMeasure updatedUnitOfMeasure = unitOfMeasureRepository.saveAndFlush(exisitingUnitOfMeasure);

        return updatedUnitOfMeasure;
    }

    public void deleteMultiple(List<UnitOfMeasure> unitsOfMeasure) {
        unitOfMeasureRepository.delete(unitsOfMeasure);
    }

    public void delete(Long id) {
        unitOfMeasureRepository.delete(id);
    }

}
