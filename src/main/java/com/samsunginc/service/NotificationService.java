package com.samsunginc.service;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public abstract class NotificationService<T extends NotificationService.NotificationConfiguration> {

    interface NotificationConfigurationProvider<T> {
        T createNotificationConfiguration();
    }

    interface NotificationConfiguration {

    }

    abstract void sendNotification(NotificationConfigurationProvider<T> configuration);
}
