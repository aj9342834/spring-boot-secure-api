package com.samsunginc.service;

import com.samsunginc.model.Currency;
import com.samsunginc.repository.CurrencyRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Service
public class CurrencyService {
    @Autowired
    private CurrencyRepository currencyRepository;

    public Currency findOne(Long uid) {
        return currencyRepository.findOne(uid);
    }
    
    public List<Currency> list()
    {
        List<Currency> currencies = currencyRepository.findAll();
        return currencies;
    }

}
