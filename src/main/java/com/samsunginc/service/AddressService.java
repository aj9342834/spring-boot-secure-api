package com.samsunginc.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.samsunginc.model.Address;
import com.samsunginc.repository.AddressRepository;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Service
public class AddressService {

    @Autowired
    AddressRepository addressRepository;

    public List<Address> list() {

        List<Address> addresses = addressRepository.findAll();

        return addresses;
    }

    public Address findOne(Long id) {
        return addressRepository.findOne(id);
    }

    public List<Address> addMultiple(List<Address> addresses) {

        List<Address> savedAddresss = addressRepository.save(addresses);

        return savedAddresss;
    }

    public Address add(Address address) {

        Address savedAddress = addressRepository.saveAndFlush(address);

        return savedAddress;
    }

    public Address update(Long id, Address address) {

        Address exisitingAddress = addressRepository.findOne(id);
        
        if (exisitingAddress == null) {
            return null;
        }
        
        BeanUtils.copyProperties(address, exisitingAddress, "uid", "uuid");
        Address updatedAddress = addressRepository.saveAndFlush(exisitingAddress);

        return updatedAddress;
    }

    public void deleteMultiple(List<Address> addresses) {
        addressRepository.delete(addresses);
    }

    public void delete(Long id) {
        addressRepository.delete(id);
    }

}
