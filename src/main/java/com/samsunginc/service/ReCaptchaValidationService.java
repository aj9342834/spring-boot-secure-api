package com.samsunginc.service;

import com.samsunginc.config.CaptchaSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestOperations;

import java.net.URI;
import java.util.regex.Pattern;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Service
public class ReCaptchaValidationService {

    @Autowired
    private CaptchaSettings captchaSettings;

    @Autowired
    private RestOperations restTemplate;

    protected static final Pattern CAPTCHA_RESPONSE_PATTERN =  Pattern.compile("[A-Za-z0-9_-]+");


    /**
     * Validates if the provided {@code response} matches the user data entered in a previous step
     * is valid by making a REST method call to the <i>Google's reCaptcha</i> REST API
     *
     * @param response the result of the user interaction with the <i>reCaptcha</i> plugin from the <i>UI</i>
     * @throws UserRegistrationValidationService.InvalidReCaptchaException if the {@code response} contains invalid characters
     * @throws UserRegistrationValidationService.ReCaptchaInvalidException if the {@code response} content was not successfully validated
     */
    public void processReCaptchaResponse(String response) {
        if(!responseSanityCheck(response)) {
            throw new UserRegistrationValidationService.InvalidReCaptchaException("CaptchaInvalidCharacters");
        }

        URI verifyUri = URI.create(String.format(
                "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s",
                captchaSettings.getSecret(), response));

        UserRegistrationValidationService.GoogleResponse googleResponse = restTemplate.getForObject(verifyUri, UserRegistrationValidationService.GoogleResponse.class);

        if(!googleResponse.isSuccess()) {
            throw new UserRegistrationValidationService.ReCaptchaInvalidException("CaptchaNotValidated");
        }
    }

    private boolean responseSanityCheck(String response) {
        return StringUtils.hasLength(response) && CAPTCHA_RESPONSE_PATTERN.matcher(response).matches();
    }
}
