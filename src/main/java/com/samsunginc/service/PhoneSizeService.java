package com.samsunginc.service;

import com.samsunginc.model.PhoneSize;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.samsunginc.repository.PhoneSizeRepository;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Service
public class PhoneSizeService implements ServiceInterface {
    @Autowired
    private PhoneSizeRepository containerSizeRepository;

    public List<PhoneSize> list() {

        List<PhoneSize> containerSizes = containerSizeRepository.findAll();

        return containerSizes;
    }

    public PhoneSize findOne(Long id) {
        return containerSizeRepository.findOne(id);
    }

    public List<PhoneSize> addMultiple(List<PhoneSize> containerSizes) {

        List<PhoneSize> savedContainerSizes = containerSizeRepository.save(containerSizes);

        return savedContainerSizes;
    }

    public PhoneSize add(PhoneSize containerSize) {

        PhoneSize savedContainerSize = containerSizeRepository.saveAndFlush(containerSize);

        return savedContainerSize;
    }

    public PhoneSize update(Long id, PhoneSize containerSize) {
        PhoneSize exisitingContainerSize = containerSizeRepository.findOne(id);
        
        if (exisitingContainerSize == null) {
            return null;
        }
        
        BeanUtils.copyProperties(containerSize, exisitingContainerSize, "uid", "uuid");
        PhoneSize updatedContainerSize = containerSizeRepository.saveAndFlush(exisitingContainerSize);

        return updatedContainerSize;
    }

    public void deleteMultiple(List<PhoneSize> containerSizes) {
        containerSizeRepository.delete(containerSizes);
    }

    public void delete(Long id) {
        containerSizeRepository.delete(id);
    }
}
