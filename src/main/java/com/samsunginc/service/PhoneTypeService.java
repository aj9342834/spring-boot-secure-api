package com.samsunginc.service;
import com.samsunginc.model.PhoneType;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.samsunginc.repository.PhoneTypeRepository;

/**
 *
 * @author Andrew
 */
@Service
public class PhoneTypeService implements ServiceInterface {
    @Autowired
    private PhoneTypeRepository containerTypeRepository;
    /**
     * Returns Container Type Records
     * @return Container Type
     */
    public List<PhoneType> list() {

        List<PhoneType> containerTypes = containerTypeRepository.findAll();

        return containerTypes;
    }
    /**
     * Find a Container Type
     * @param id
     * @return 
     */
    public PhoneType findOne(Long id) {
        return containerTypeRepository.findOne(id);
    }

    /**
     * Add a Container Type Record.
     * @param containerType Container Type Record to be added
     * @return Container Type record that was added
     */
    public PhoneType add(PhoneType containerType) {

        PhoneType savedContainerType = containerTypeRepository.saveAndFlush(containerType);

        return savedContainerType;
    }
    /**
     * Update Existing Container Type
     * @param id unique identifier of existing Group
     * @param containerType Container Type values to be updated
     * @return Updated Container Type
     */
    public PhoneType update(Long id, PhoneType containerType) {
        PhoneType exisitingContainerType = containerTypeRepository.findOne(id);
        
        if (exisitingContainerType == null) {
            return null;
        }
        
        BeanUtils.copyProperties(containerType, exisitingContainerType, "uid", "uuid");
        PhoneType updatedContainerType = containerTypeRepository.saveAndFlush(exisitingContainerType);

        return updatedContainerType;
    }
    /**
     * Delete specified Container Type record
     * @param id unique identifier
     */
    public void delete(Long id) {
        containerTypeRepository.delete(id);
    }
}
