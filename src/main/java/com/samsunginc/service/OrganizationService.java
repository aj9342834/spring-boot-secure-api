package com.samsunginc.service;

import com.samsunginc.model.Organization;
import com.samsunginc.repository.OrganizationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for the {@link Organization} entities related operations.
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Service
public class OrganizationService {

    @Autowired
    private OrganizationRepository organizationRepository;

    /**
     * Returns all the {@link Organization} objects
     * @return a {@link List} of {@link Organization} objects
     */
    public List<Organization> list() {
        return organizationRepository.findAll();
    }

    /**
     * Return an {@link Organization} given its identifier
     * @param uid the {@link Organization} identifier
     * @return the {@link Organization} with the given {@code uid}
     */
    public Organization findOne(Long uid) {
        return organizationRepository.findOne(uid);
    }
}
