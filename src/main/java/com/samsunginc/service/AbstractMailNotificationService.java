package com.samsunginc.service;

import com.samsunginc.model.User;
import com.samsunginc.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class provides an interface for email based notification services.<br/>
 * The child classes should call the {@code sendNotification} method in order to send the email notification.
 * @author andrew jackson
 */
@Service
public abstract class AbstractMailNotificationService<T extends AbstractMailNotificationService.MailNotificationConfiguration> extends NotificationService<T> {

    private final Logger logger = LoggerFactory.getLogger(AbstractMailNotificationService.class);
    private final JavaMailSender javaMailSender;
    private final MessageSource messageSource;
    private final TemplateEngine templateEngine;

    @Value("${samsung.mail.admin.email}")
    private String adminEmailAddress;

    @Value("${samsung.mail.defaultsender.address}")
    private String systemNoReplySenderAddress;

    @Value("${samsung.mail.defaultsender.name}")
    private String systemNoReplySenderName;

    @Value("${spring.mail.default-encoding}")
    private String defaultEncoding;

    private final UserRepository userRepository;

    @Autowired
    protected AbstractMailNotificationService(JavaMailSender javaMailSender, MessageSource messageSource, TemplateEngine templateEngine, UserRepository userRepository) {
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
        this.userRepository = userRepository;
    }

    /**
     * Sends a mail notification based on the configured attributes of {@code createNotificationConfiguration} method
     */
    @Override
    protected final void sendNotification(NotificationConfigurationProvider<T> configurationProvider) {
        sendNotificationWithErrorNotification(configurationProvider.createNotificationConfiguration(), true);
    }

    @Async
    private void sendNotificationWithErrorNotification(MailNotificationConfiguration configuration, boolean notifyAdmin) {
        try {
            if (configuration instanceof MultipartMailNotificationConfiguration) {
                sendMail((MultipartMailNotificationConfiguration) configuration);
            } else {
                sendMail(configuration);
            }
        } catch (MailException e) {
            if (notifyAdmin) {
                MailNotificationConfiguration errorNotificationConfiguration = setupErrorNotificationConfiguration(configuration, e);
                sendNotificationWithErrorNotification(errorNotificationConfiguration, false);
            }
            logger.info(e.getMessage());
        } catch (MessagingException e) {
            logger.info(e.getMessage());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void sendMail(MailNotificationConfiguration configuration) throws MessagingException {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(configuration.getFrom());
        simpleMailMessage.setTo(configuration.getTo());
        simpleMailMessage.setSubject(configuration.getSubject());
        simpleMailMessage.setText(configuration.getText());

        this.javaMailSender.send(simpleMailMessage);
    }

    private void sendMail(MultipartMailNotificationConfiguration configuration) throws MessagingException, UnsupportedEncodingException {
        MimeMessage mimeMessage = this.javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, configuration.getEncoding());
        helper.setFrom(configuration.getFrom());
        helper.setBcc(configuration.getTo());
        helper.setSubject(configuration.getSubject());
        helper.setText(configuration.getText(), configuration.getMultiPartText());

        this.javaMailSender.send(mimeMessage);
    }

    private MailNotificationConfiguration setupErrorNotificationConfiguration(MailNotificationConfiguration configuration, MailException e) {
        String failureSubject = messageSource.getMessage("mail.notification.delivery.failure.subject", null, Locale.ENGLISH);
        Map<String, String> params = new HashMap<>();
        params.put("recipient", configuration.getUser().getFirstName() + " " + configuration.getUser().getLastName());
        params.put("email", configuration.getTo()[0]);
        params.put("subject", configuration.getSubject());
        params.put("rejectReason", e.getMessage());
        String failureBody = buildMessageBodyFromTemplate(params, "mail/html/notificationDeliveryFailure");
        String[] adminEmailAddress = getSystemAdministratorsEmails();
        return new MailNotificationConfiguration(
                adminEmailAddress, getSystemNoReplySenderName(), getSystemNoReplySenderAddress(), failureSubject, failureBody, configuration.getUser());
    }

    String[] getSystemAdministratorsEmails() {
        List<User> systemAdministrators = getSystemAdministrators();
        String[] adminEmails = new String[systemAdministrators.size()];
        for (int i = 0; i < systemAdministrators.size(); i++) {
            User admin = systemAdministrators.get(i);
            adminEmails[i] = admin.getEmail();
        }
        return adminEmails;
    }

    List<User> getSystemAdministrators() {
        return userRepository.findSystemAdministrators();
    }

    String buildMessageBodyFromTemplate(Map<String, String> variables, String template) {
        Context context = new Context();
        for (String var : variables.keySet()) {
            context.setVariable(var, variables.get(var));
        }
        return templateEngine.process(template, context);
    }

    String getSystemNoReplySenderAddress() {
        return systemNoReplySenderAddress;
    }

    String getAdminEmailAddress() {
        return adminEmailAddress;
    }

    String getSystemNoReplySenderName() {
        return systemNoReplySenderName;
    }

    String getMessage(String key, String...params) {
        return messageSource.getMessage(key, params, Locale.ENGLISH);
    }

    String getDefaultEncoding() {
        return defaultEncoding;
    }

    /**
     * Contains the info necessary to build a mail message
      */

    static class MailNotificationConfiguration implements NotificationService.NotificationConfiguration {
        private final String[] to;
        private final String senderName;
        private final String from;
        private final String subject;
        private final String text;
        private final User user;

        MailNotificationConfiguration(String[] to, String senderName, String from, String subject, String text, User user) {
            this.to = to;
            this.senderName = senderName;
            this.from = from;
            this.subject = subject;
            this.text = text;
            this.user = user;
        }

        public String[] getTo() {
            return to;
        }

        public String getFrom() {
            return from;
        }

        public String getSubject() {
            return subject;
        }

        public String getText() {
            return text;
        }

        public String getSenderName() {
            return senderName;
        }

        public User getUser() {
            return user;
        }
    }

    static class MultipartMailNotificationConfiguration extends MailNotificationConfiguration {
        private final String multiPartText;
        private final String encoding;

        MultipartMailNotificationConfiguration(String[] to, String senderName, String from, String subject, String text, String multiPartText, String encoding, User user) {
            super(to, senderName, from, subject, text, user);
            this.multiPartText = multiPartText;
            this.encoding = encoding;
        }

        String getMultiPartText() {
            return multiPartText;
        }

        String getEncoding() {
            return encoding;
        }
    }
}
