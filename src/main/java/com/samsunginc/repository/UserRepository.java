package com.samsunginc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.samsunginc.model.User;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    int countByEmail(String email);
    int countByUsername(String username);

    @Query("select u from User u join u.roles r where r.name = 'ROLE_ADMIN'")
    List<User> findSystemAdministrators();
}
