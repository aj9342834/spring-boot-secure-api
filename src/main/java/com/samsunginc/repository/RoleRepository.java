package com.samsunginc.repository;

import com.samsunginc.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
    public Role findByName(String name);
}
