package com.samsunginc.repository;

import com.samsunginc.model.PhoneType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Andrew Jackson <aj9342@gmail.com>
 * 
 * This class manages data access to persistent storage for Container Type
 * @see PhoneType
 */
public interface PhoneTypeRepository extends JpaRepository<PhoneType, Long> {
    
}
