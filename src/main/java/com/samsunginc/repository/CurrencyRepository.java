package com.samsunginc.repository;

import com.samsunginc.model.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public interface CurrencyRepository extends JpaRepository<Currency, Long> {

}
