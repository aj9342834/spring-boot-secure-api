//package com.samsunginc.repository;
//
//import com.samsunginc.model.PhoneSize;
//import com.samsunginc.model.PhoneType;
//import com.samsunginc.model.DemurrageDetentionDetail;
//import com.samsunginc.model.DemurrageDetentionDetail.DemurrageDetentionStatus;
//import com.samsunginc.model.DemurrageDetentionRate;
//import com.samsunginc.model.FreeDaysRuleHeader;
//import com.samsunginc.model.FreeDaysRuleHeader.RuleType;
//import com.samsunginc.model.Party;
//import com.samsunginc.model.RateLevel;
//import java.time.LocalDateTime;
//import java.util.List;
//import javax.transaction.Transactional;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Modifying;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//
///**
// * @author Andrew Jackson <aj9342@gmail.com>
// */
//public interface DemurrageDetentionDetailRepository extends JpaRepository<DemurrageDetentionDetail, Long> {
//
//    /**
//     * Delete a demurrage & detention record
//     * @param id - Unique identifier 
//     */
//    @Modifying
//    @Transactional
//    @Query(value = "delete from DemurrageDetentionRate a where a.demurrageDetentionDetail.uid = ?1")
//    void removeRateByDemurrageDetention(long id);
//    /**
//     * Search demurrage & detention Rates by the following parameters
//     * @param rateLevel - Assigned level to demurrage & detention attribute (agents, consignee, container-size, etc) 
//     * @param status - Status of rate
//     * @param filter - Filter by the search team
//     * @return 
//     */
//    @Query("select d from DemurrageDetentionDetail d"
//            + " join d.rates r "
//            + " left join d.shippingLine sl "
//            + " left join d.consignee c "
//            + " left join d.shipper s "
//            + " left join d.agent a "
//            + " left join d.containerType ct "
//            + " left join d.containerSize cs "
//            + " left join d.tradeLane tl "
//            + " where d.rateLevel.rateLevel <= :level and"
//            + " (:status is null or d.demurrageDetentionStatus = :status) and"
//            + " (:filter is null or ((sl is not null and UPPER(d.shippingLine.name) like :filter) or "
//            + " (c is not null and UPPER(d.consignee.name) like :filter) or  "
//            + " (s is not null and UPPER(d.shipper.name) like :filter) or "
//            + " (c is not null and UPPER(d.agent.name) like :filter) or "
//            + " (cs is not null and UPPER(d.containerSize.name) like :filter) or  "
//            + " (ct is not null and UPPER(d.containerType.description) like :filter) or "
//            + " (tl is not null and UPPER(d.tradeLane.tradeLane) like :filter)))"
//            + " group by d.uid"
//            + " order by d.uid desc")
//    List<DemurrageDetentionDetail> findAllRatesByElements(@Param("level") int rateLevel,
//            @Param("status") DemurrageDetentionStatus status,
//            @Param("filter") String filter);
//    /**
//     * Search demurrage & detention Deposit-Rates by the following parameters
//     * @param rateLevel - Assigned level to demurrage & detention attribute (agents, consignee, container-size, etc) 
//     * @param status - Status of deposit-rate
//     * @param filter - Filter by the search team
//     * @return 
//     */
//    @Query("select d from DemurrageDetentionDetail d"
//            + " join d.depositRates r "
//            + " left join d.shippingLine sl "
//            + " left join d.consignee c "
//            + " left join d.shipper s "
//            + " left join d.agent a "
//            + " left join d.containerType ct "
//            + " left join d.containerSize cs "
//            + " left join d.tradeLane tl "
//            + " where d.rateLevel.rateLevel <= :level and"
//            + " (:status is null or d.demurrageDetentionStatus = :status) and"
//            + " (:filter is null or ((sl is not null and UPPER(d.shippingLine.name) like :filter) or "
//            + " (c is not null and UPPER(d.consignee.name) like :filter) or  "
//            + " (s is not null and UPPER(d.shipper.name) like :filter) or "
//            + " (c is not null and UPPER(d.agent.name) like :filter) or "
//            + " (cs is not null and UPPER(d.containerSize.name) like :filter) or  "
//            + " (ct is not null and UPPER(d.containerType.description) like :filter) or "
//            + " (tl is not null and UPPER(d.tradeLane.tradeLane) like :filter)))"
//            + " group by d.uid"
//            + " order by d.uid desc")
//    List<DemurrageDetentionDetail> findAllDepositRatesByElements(@Param("level") int rateLevel,
//            @Param("status") DemurrageDetentionStatus status,
//            @Param("filter") String filter);
//     /**
//     * Search demurrage & detention free-day Rules by the following parameters
//     * @param rateLevel - Assigned level to demurrage & detention attribute (agents, consignee, container-size, etc) 
//     * @param status - Status of rules
//     * @param ruleType - Rule type (Basic, Commodity, Volume)
//     * @param filter - Filter by the search team
//     * @return 
//     */
//    @Query("select d from DemurrageDetentionDetail d"
//            + " join d.freeDaysRuleHeader r "
//            + " left join d.shippingLine sl "
//            + " left join d.consignee c "
//            + " left join d.shipper s "
//            + " left join d.agent a "
//            + " left join d.containerType ct "
//            + " left join d.containerSize cs "
//            + " left join d.tradeLane tl "
//            + " where d.rateLevel.rateLevel <= :level and"
//            + " (:status is null or d.demurrageDetentionStatus = :status) and"
//            + " ( :ruleType is null or r.ruleType = :ruleType) and"
//            + " (:filter is null or ((sl is not null and UPPER(d.shippingLine.name) like :filter) or "
//            + " (c is not null and UPPER(d.consignee.name) like :filter) or  "
//            + " (s is not null and UPPER(d.shipper.name) like :filter) or "
//            + " (c is not null and UPPER(d.agent.name) like :filter) or "
//            + " (cs is not null and UPPER(d.containerSize.name) like :filter) or  "
//            + " (ct is not null and UPPER(d.containerType.description) like :filter) or "
//            + " (tl is not null and UPPER(d.tradeLane.tradeLane) like :filter)))"
//            + " group by d.uid"
//            + " order by d.uid desc")
//    List<DemurrageDetentionDetail> findAllRulesByElements(@Param("level") int rateLevel,
//            @Param("status") DemurrageDetentionStatus status,
//            @Param("ruleType") RuleType ruleType,
//            @Param("filter") String filter);
//    /**
//     * Return all demurrage & detention rates
//     * @return 
//     */
//    @Query("select d from DemurrageDetentionDetail d"
//            + " join d.rates r"
//            + " group by d.uid"
//            + " order by d.uid desc")
//    List<DemurrageDetentionDetail> findAllRates();
//     /**
//     * Return all demurrage & detention deposit-rates
//     * @return 
//     */
//    @Query("select d from DemurrageDetentionDetail d"
//            + " join d.depositRates r"
//            + " group by d.uid"
//            + " order by d.uid desc")
//    List<DemurrageDetentionDetail> findAllDepositRates();
//     /**
//     * Return all demurrage & detention free-days rules
//     * @return 
//     */
//    @Query("select d from DemurrageDetentionDetail d"
//            + " join d.freeDaysRuleHeader r"
//            + " group by d.uid"
//            + " order by d.uid desc")
//    List<DemurrageDetentionDetail> findAllFreeDaysRules();
//
//    /**
//     *
//     * @param demurrageDetentionStatus
//     * @param id
//     */
//    @Modifying
//    @Transactional
//    @Query("update DemurrageDetentionDetail d"
//            + " set d.demurrageDetentionStatus = :status"
//            + " where d.uid in (:id)")
//    void updateStatus(@Param("status") DemurrageDetentionStatus demurrageDetentionStatus, @Param("id") List<Long> id);
//
//}
