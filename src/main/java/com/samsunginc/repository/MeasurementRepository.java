package com.samsunginc.repository;

import com.samsunginc.model.Measurement;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public interface MeasurementRepository extends JpaRepository<Measurement, Long> {
    
}
