package com.samsunginc.repository;

import com.samsunginc.model.Organization;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public interface OrganizationRepository extends JpaRepository<Organization, Long> {
}
