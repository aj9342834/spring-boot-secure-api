package com.samsunginc.repository;

import com.samsunginc.model.Phone;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public interface PhoneRepository extends JpaRepository<Phone, Long> {
  
    /**
     *
     * @param id
     * @return Phone
     */
    Optional<Phone> findByUid(Long id);

}
