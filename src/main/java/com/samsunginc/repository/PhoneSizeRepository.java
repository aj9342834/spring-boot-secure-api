package com.samsunginc.repository;

import com.samsunginc.model.PhoneSize;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public interface PhoneSizeRepository extends JpaRepository<PhoneSize, Long> {
    
}
