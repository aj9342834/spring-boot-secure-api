package com.samsunginc.repository;

import com.samsunginc.model.UnitOfMeasure;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public interface UnitOfMeasureRepository extends JpaRepository<UnitOfMeasure, Long> {
    
}
