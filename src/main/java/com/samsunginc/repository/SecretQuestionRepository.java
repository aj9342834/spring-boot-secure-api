package com.samsunginc.repository;

import com.samsunginc.model.SecretQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public interface SecretQuestionRepository extends JpaRepository<SecretQuestion, Long> {
}
