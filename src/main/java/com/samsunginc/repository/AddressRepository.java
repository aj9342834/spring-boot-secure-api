package com.samsunginc.repository;

import com.samsunginc.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public interface AddressRepository extends JpaRepository<Address, Long> {
    
}
