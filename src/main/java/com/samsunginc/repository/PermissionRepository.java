package com.samsunginc.repository;

import com.samsunginc.model.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public interface PermissionRepository extends JpaRepository<Permission, Long> {
    public Permission findByName(String name);
}
