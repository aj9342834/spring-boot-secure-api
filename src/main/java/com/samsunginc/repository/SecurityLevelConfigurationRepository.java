package com.samsunginc.repository;

import com.samsunginc.model.SecurityLevelConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;

/** Repository class for {@link SecurityLevelConfiguration} objects
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public interface SecurityLevelConfigurationRepository extends JpaRepository<SecurityLevelConfiguration, Long> {

}
