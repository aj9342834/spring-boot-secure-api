package com.samsunginc.repository;

import com.samsunginc.model.MeasurementInformation;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public interface MeasurementInformationRepository extends JpaRepository<MeasurementInformation, Long> {
    
}
