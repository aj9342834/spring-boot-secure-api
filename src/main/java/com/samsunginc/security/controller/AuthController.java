package com.samsunginc.security.controller;

import com.samsunginc.model.SecurityLevelConfiguration;
import com.samsunginc.service.ReCaptchaValidationService;
import com.samsunginc.service.SecurityLevelConfigurationService;
import com.samsunginc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;

/**
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
@RestController
@RequestMapping(value = "/api/${samsung.api.version}/auth")
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private ReCaptchaValidationService reCaptchaValidationService;

    @Autowired
    private SecurityLevelConfigurationService configurationService;

    @RequestMapping(value="login", method=RequestMethod.GET)
    public ResponseEntity<UserCredentials> authentication(Authentication authentication) {
        Object principal = authentication.getPrincipal();
        if (principal instanceof User) {
            User principalUser = (User) principal;
            com.samsunginc.model.User user = userService.findByUsername(principalUser.getUsername());
            UserCredentials credentials = UserCredentials.fromUser(user, principalUser.getAuthorities());

            return ok(credentials);
        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "recaptcha", method = RequestMethod.POST)
    public ResponseEntity<String> verifyCaptcha(@RequestParam(name = "re-capthca-response") String reCaptchaResponse) {
        try {
            reCaptchaValidationService.processReCaptchaResponse(reCaptchaResponse);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "security-configuration", method = RequestMethod.GET)
    public ResponseEntity<SecurityLevelConfiguration> getSecurityLevelConfiguration() {
        return new ResponseEntity<>(configurationService.getSecurityConfiguration(), HttpStatus.OK);
    }


    private static class UserCredentials {
        private String username;
        private String firstName;
        private String lastName;
        private String email;
        private String middleInitial;
        private List<String> authorities;

        // TODO: must remove
        // necessary due to basic http authentication
        private String password;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMiddleInitial() {
            return middleInitial;
        }

        public void setMiddleInitial(String middleInitial) {
            this.middleInitial = middleInitial;
        }

        public List<String> getAuthorities() {
            return authorities;
        }

        public void setAuthorities(List<String> authorities) {
            this.authorities = authorities;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public static UserCredentials fromUser(com.samsunginc.model.User user, Collection<GrantedAuthority> authorities) {
            UserCredentials credentials = new UserCredentials();
            credentials.setUsername(user.getUsername());
            credentials.setFirstName(user.getFirstName());
            credentials.setLastName(user.getLastName());
            credentials.setEmail(user.getEmail());
            credentials.setMiddleInitial(user.getMiddleInitial());
            credentials.setAuthorities(authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()));
            credentials.setPassword(user.getPassword());
            return credentials;
        }
    }
}
