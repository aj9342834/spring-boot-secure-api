package com.samsunginc.validator;

import com.samsunginc.model.User;
import com.samsunginc.service.UserRegistrationValidationService;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
public class UserValidator implements Validator {

    private final UserRegistrationValidationService validationService;
    private boolean validatePassword = true;
    private boolean validateUsername = true;
    private boolean validateEmail = true;

    public UserValidator(UserRegistrationValidationService validationService) {
        this.validationService = validationService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.isAssignableFrom(aClass);
    }
    
    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;

        List<UserRegistrationValidationService.HasFieldError> validations = new ArrayList<>();
        validations.add(validationService.validateFirstName(user));
        validations.add(validationService.validateLastName(user));
        validations.add(validationService.validateUsername(user));
        validations.add(validationService.validateUserEmail(user));
        validations.add(validationService.validateUserPassword(user));

        for (UserRegistrationValidationService.HasFieldError v : validations) {
            for (String error : v.getErrors()) {
                errors.rejectValue(v.getField(), error);
            }
        }

        if (errors.hasErrors()) {
            throw new ValidationException(errors.getAllErrors());
        }
    }
}
