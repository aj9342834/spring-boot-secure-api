package com.samsunginc.validator;

import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.ObjectError;

/**
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
public class ValidationException extends javax.validation.ValidationException {
    List<ObjectError> errors = new ArrayList<>();

    public ValidationException(List<ObjectError> errors) {
        this.errors.addAll(errors);
    }

    public List<ObjectError> getErrors() {
        return errors;
    }

    public void setErrors(List<ObjectError> errors) {
        this.errors = errors;
    }
    
}
