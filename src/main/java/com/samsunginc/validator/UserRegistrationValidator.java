package com.samsunginc.validator;

import com.samsunginc.controller.UserRegistrationController;
import com.samsunginc.service.ReCaptchaValidationService;
import com.samsunginc.service.UserRegistrationValidationService;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
public class UserRegistrationValidator implements Validator {

    private final UserRegistrationValidationService validationService;
    private final ReCaptchaValidationService reCaptchaValidationService;

    public UserRegistrationValidator(UserRegistrationValidationService validationService, ReCaptchaValidationService reCaptchaValidationService) {
        this.validationService = validationService;
        this.reCaptchaValidationService = reCaptchaValidationService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UserRegistrationController.UserRegistration.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserRegistrationController.UserRegistration userRegistration = (UserRegistrationController.UserRegistration) o;

        List<UserRegistrationValidationService.HasFieldError> validations = new ArrayList<>();
        validations.add(validationService.validateFirstName(userRegistration.getUser()));
        validations.add(validationService.validateLastName(userRegistration.getUser()));
        validations.add(validationService.validateUsername(userRegistration.getUser()));
        validations.add(validationService.validateUserEmail(userRegistration.getUser()));
        validations.add(validationService.validateUserPassword(userRegistration.getUser()));

        validations.addAll(processReCaptchaValidation(userRegistration));

        for (UserRegistrationValidationService.HasFieldError v : validations) {
            for (String error : v.getErrors()) {
                errors.rejectValue("captchaResponse".equals(v.getField()) ? v.getField() : "user." + v.getField(), error);
            }
        }

        if (errors.hasErrors()) {
            throw new ValidationException(errors.getAllErrors());
        }
    }

    private List<UserRegistrationValidationService.HasFieldError> processReCaptchaValidation(UserRegistrationController.UserRegistration userRegistration) {
        List<UserRegistrationValidationService.HasFieldError> reCaptchaValidations = new ArrayList<>();
        try {
            reCaptchaValidationService.processReCaptchaResponse(userRegistration.getCaptchaResponse());
        } catch (UserRegistrationValidationService.InvalidReCaptchaException e) {
            reCaptchaValidations.add(createHasFieldErrorForCaptcha(e));
        } catch (UserRegistrationValidationService.ReCaptchaInvalidException e) {
            reCaptchaValidations.add(createHasFieldErrorForCaptcha(e));
        }
        return reCaptchaValidations;
    }

    private UserRegistrationValidationService.HasFieldError createHasFieldErrorForCaptcha(final Exception e) {
        return new UserRegistrationValidationService.HasFieldError() {
            @Override
            public String getField() {
                return "captchaResponse";
            }

            @Override
            public List<String> getErrors() {
                return Arrays.asList(e.getMessage());
            }
        };
    }
}
