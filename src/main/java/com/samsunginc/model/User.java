package com.samsunginc.model;

import com.samsunginc.deserializer.OrganizationListDeserializer;
import com.samsunginc.deserializer.SecretQuestionDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
/**
 *
 * @author andrew jackson
 */
@Entity
@Audited
@Table(name = "_USER")
public class User extends DomainObject<User> {

    @Column(name = "USERNAME", unique = true)
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "USER_STATUS")
    @Enumerated(EnumType.STRING)
    private UserStatus userStatus;
    
    @ManyToMany
    @JoinTable( 
        name = "_USER_ROLES", 
        joinColumns = @JoinColumn(name = "USER_ID", referencedColumnName = "UID"), 
        inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "UID")) 
    private List<Role> roles;

    private String email;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "MIDDLE_INITIAL")
    private String middleInitial;

    @ManyToMany
    @JoinTable(
            name = "_ORGANIZATION_USER_REGISTRATIONS",
            joinColumns = @JoinColumn(name = "USER_REGISTRATION", referencedColumnName = "UID"),
            inverseJoinColumns = @JoinColumn(name = "ORGANIZATION", referencedColumnName = "UID"))
    @JsonDeserialize(using = OrganizationListDeserializer.class)
    private List<Organization> organizations;

    @ManyToOne
    @JsonDeserialize(using = SecretQuestionDeserializer.class)
    private SecretQuestion secretQuestion;

    @Column(name = "SECRET_QUESTION_ANSWER")
    private String secretQuestionAnswer;

    @Column(name = "USER_REGISTRATION_REQUEST_DATE")
    private LocalDateTime registrationDateTime;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
     
        this.password = password;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public User setRoles(List<Role> roles) {
        this.roles = roles;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public User setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public User setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getMiddleInitial() {
        return middleInitial;
    }

    public User setMiddleInitial(String middleInitial) {
        this.middleInitial = middleInitial;
        return this;
    }

    public List<Organization> getOrganizations() {
        return organizations;
    }

    public User setOrganizations(List<Organization> organizations) {
        this.organizations = organizations;
        return this;
    }

    public SecretQuestion getSecretQuestion() {
        return secretQuestion;
    }

    public User setSecretQuestion(SecretQuestion secretQuestion) {
        this.secretQuestion = secretQuestion;
        return this;
    }

    public String getSecretQuestionAnswer() {
        return secretQuestionAnswer;
    }

    public User setSecretQuestionAnswer(String secretQuestionAnswer) {
        this.secretQuestionAnswer = secretQuestionAnswer;
        return this;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    @Override
    protected User getThis() {
        return this;
    }

    public LocalDateTime getRegistrationDateTime() {
        return registrationDateTime;
    }

    public void setRegistrationDateTime(LocalDateTime userRegistrationRequestDate) {
        this.registrationDateTime = userRegistrationRequestDate;
    }

    public boolean isActive() {
        return userStatus != null && userStatus == UserStatus.ACTIVE;
    }
}
