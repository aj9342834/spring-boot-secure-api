package com.samsunginc.model;

import com.samsunginc.deserializer.UnitOfMeasureDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 * @author andrew jackson
 */
@Entity
@Audited
@Table(name = "_MEASUREMENT")
public class Measurement extends DomainObject<Measurement> {    
    @ManyToOne
    private UnitOfMeasure unitOfMeasure;
    @Column(name="MEASUREMENT_VALUE")
    private Double measurementValue;

    public UnitOfMeasure getUnitOfMeasure() {
        return unitOfMeasure;
    }

    @JsonDeserialize(using = UnitOfMeasureDeserializer.class)
    public Measurement setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
        return this;
    }

    public Double getMeasurementValue() {
        return measurementValue;
    }

    public Measurement setMeasurementValue(Double measurementValue) {
        this.measurementValue = measurementValue;
        return this;
    }

    @Override
    protected Measurement getThis() {
        return this;
    }
}
