package com.samsunginc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 * This domain object represents UN-recognized units of measure.
 * @see http://www.unece.org/cefact/codesfortrade/codes_index.html
 * @author andrew jackson
 */
@Entity
@Audited
@Table(name = "_UNIT_OF_MEAUSRE")
public class UnitOfMeasure extends DomainObject<UnitOfMeasure> {
    @Column(name="NAME")
    String name;
    @Column(name="ISO_CODE")
    String isoCode;
    @Column(name="ISO_SYMBOL")
    String isoSymbol;

    public String getName() {
        return name;
    }

    public UnitOfMeasure setName(String name) {
        this.name = name;
        return this;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public UnitOfMeasure setIsoCode(String isoCode) {
        this.isoCode = isoCode;
        return this;
    }

    public String getIsoSymbol() {
        return isoSymbol;
    }

    public UnitOfMeasure setIsoSymbol(String isoSymbol) {
        this.isoSymbol = isoSymbol;
        return this;
    }
    
    @Override
    protected UnitOfMeasure getThis() {
        return this;
    }
}
