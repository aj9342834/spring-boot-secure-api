package com.samsunginc.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 * @author andrew jackson
 */
@Entity
@Audited
@Table(name = "_PHONE_SIZE")
public class PhoneSize extends DomainObject<PhoneSize> {
    @Column(name = "SERIAL_NUMBER")
    private String name;
    @OneToOne(cascade = CascadeType.ALL)
    private MeasurementInformation measurementInformation;

    public String getName() {
        return name;
    }

    public PhoneSize setName(String name) {
        this.name = name;
        return this;
    }

    public MeasurementInformation getMeasurementInformation() {
        return measurementInformation;
    }

    public PhoneSize setMeasurementInformation(MeasurementInformation measurementInformation) {
        this.measurementInformation = measurementInformation;
        return this;
    }
    
    @Override
    protected PhoneSize getThis() {
        return this;
    }
}
