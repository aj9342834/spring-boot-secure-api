package com.samsunginc.model;

/**
 *
 * @author andrew jackson
 */
public enum UserStatus {
    PENDING, ACTIVE, INACTIVE, REJECTED
}
