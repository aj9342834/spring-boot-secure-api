package com.samsunginc.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlTransient;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * @author andrew jackson <aj9342@gmail.com>
 * @param <T>
 */
@MappedSuperclass
@Configurable(autowire = Autowire.BY_TYPE)
public abstract class DomainObject <T extends DomainObject<T>> implements Serializable {
    @Id
    @Column(name = "UID")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long uid;
    
    @Column(name = "UUID", columnDefinition = "char(36)", length = 36, unique = true)
    String uuid;

    public Long getUid() {
        return uid;
    }

    public T setUid(Long uid) {
        this.uid = uid;
        return getThis();
    }

    public String getUuid() {
        return uuid;
    }

    public T setUuid(String uuid) {
        this.uuid = uuid;
        return getThis();
    }

    @PrePersist
    protected void onPrePersist() {
        if (this.uuid == null) {
            this.uuid = UUID.randomUUID().toString();
        }
    }

    protected abstract T getThis();
    
    @JsonIgnore
    @XmlTransient
    public Class getTypeClass() {
        return getThis().getClass();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DomainObject){
            return ((DomainObject)obj).getUid().equals(this.getUid());
        }
        return false;
    }    
}
