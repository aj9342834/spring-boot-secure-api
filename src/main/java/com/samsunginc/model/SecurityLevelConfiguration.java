package com.samsunginc.model;

import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author andrew jackson
 */
@Entity
@Audited
@Table(name = "_SECURITY_LEVEL_CONFIGURATION")
public class SecurityLevelConfiguration extends DomainObject<SecurityLevelConfiguration> {

    @Column(name = "EMAIL_AS_USER_NAME")
    private Boolean emailAsUserName = Boolean.FALSE;

    @Column(name = "REGISTRATION_VERIFICATION_REQUIRED")
    private Boolean registrationVerificationRequired = Boolean.TRUE;

    @Column(name = "CHECK_MINIMUM_LENGTH")
    private Boolean checkMinimumLength = Boolean.TRUE;

    @Column(name = "MINIMUM_PASSWORD_LENGTH")
    private int minimumPasswordLength = 10;

    @Column(name = "CHECK_VALID_PERIOD")
    private Boolean checkValidPeriod = Boolean.TRUE;

    @Column(name = "VALID_PERIOD")
    private int validPeriod = 30;

    @Column(name = "INCLUDE_SPECIAL_CHARACTERS")
    private Boolean includeSpecialCharacters = Boolean.TRUE;

    @Column(name = "INCLUDE_NUMBERS_AND_LETTERS")
    private Boolean includeNumbersAndLetters = Boolean.TRUE;

    @Column(name = "EXCLUDE_REPEATING_CHARACTERS")
    private Boolean excludeRepeatingCharacters = Boolean.TRUE;

    @Column(name = "ALLOW_MIXED_CASE")
    private Boolean allowMixedCase = Boolean.TRUE;

    @Column(name = "EXCLUDE_USERNAME_EMAIL")
    private Boolean excludeUserNameAndEmail = Boolean.TRUE;

    @Column(name = "CHECK_LOGIN_ATTEMPTS")
    private Boolean checkLoginAttempts = Boolean.TRUE;

    @Column(name = "LOGIN_ATTEMPTS_BEFORE_CAPTCHA")
    private int loginAttemptsBeforeCaptcha = 3;

    public Boolean getEmailAsUserName() {
        return emailAsUserName;
    }

    public void setEmailAsUserName(Boolean emailAsUserName) {
        this.emailAsUserName = emailAsUserName;
    }

    public Boolean getRegistrationVerificationRequired() {
        return registrationVerificationRequired;
    }

    public void setRegistrationVerificationRequired(Boolean registrationVerificationRequired) {
        this.registrationVerificationRequired = registrationVerificationRequired;
    }

    public Boolean getCheckMinimumLength() {
        return checkMinimumLength;
    }

    public void setCheckMinimumLength(Boolean checkMinimumLenght) {
        this.checkMinimumLength = checkMinimumLenght;
    }

    public int getMinimumPasswordLength() {
        return minimumPasswordLength;
    }

    public void setMinimumPasswordLength(int minimumPasswordLength) {
        this.minimumPasswordLength = minimumPasswordLength;
    }

    public Boolean getCheckValidPeriod() {
        return checkValidPeriod;
    }

    public void setCheckValidPeriod(Boolean checkValidPeriod) {
        this.checkValidPeriod = checkValidPeriod;
    }

    public int getValidPeriod() {
        return validPeriod;
    }

    public void setValidPeriod(int validPeriod) {
        this.validPeriod = validPeriod;
    }

    public Boolean getIncludeSpecialCharacters() {
        return includeSpecialCharacters;
    }

    public void setIncludeSpecialCharacters(Boolean includeSpecialCharacters) {
        this.includeSpecialCharacters = includeSpecialCharacters;
    }

    public Boolean getIncludeNumbersAndLetters() {
        return includeNumbersAndLetters;
    }

    public void setIncludeNumbersAndLetters(Boolean includeNumbersAndLetters) {
        this.includeNumbersAndLetters = includeNumbersAndLetters;
    }

    public Boolean getExcludeRepeatingCharacters() {
        return excludeRepeatingCharacters;
    }

    public void setExcludeRepeatingCharacters(Boolean excludeRepeatingCharacters) {
        this.excludeRepeatingCharacters = excludeRepeatingCharacters;
    }

    public Boolean getAllowMixedCase() {
        return allowMixedCase;
    }

    public void setAllowMixedCase(Boolean allowMixedCase) {
        this.allowMixedCase = allowMixedCase;
    }

    public Boolean getExcludeUserNameAndEmail() {
        return excludeUserNameAndEmail;
    }

    public void setExcludeUserNameAndEmail(Boolean excludeUserNameAndEmail) {
        this.excludeUserNameAndEmail = excludeUserNameAndEmail;
    }

    public Boolean getCheckLoginAttempts() {
        return checkLoginAttempts;
    }

    public void setCheckLoginAttempts(Boolean checkLoginAttempts) {
        this.checkLoginAttempts = checkLoginAttempts;
    }

    public int getLoginAttemptsBeforeCaptcha() {
        return loginAttemptsBeforeCaptcha;
    }

    public void setLoginAttemptsBeforeCaptcha(int loginAttemptsBeforeCaptcha) {
        this.loginAttemptsBeforeCaptcha = loginAttemptsBeforeCaptcha;
    }

    @Override
    protected SecurityLevelConfiguration getThis() {
        return this;
    }
}
