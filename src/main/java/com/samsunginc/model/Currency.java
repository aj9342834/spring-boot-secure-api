package com.samsunginc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author andrew jackson
 */
@Entity
@Audited
@Table(name = "_CURRENCY")
public class Currency extends DomainObject<Currency> {
    @Column(name="NAME")
    private String name;
    @Column(name="ISO_CODE")
    private String isoCode;
    @Column(name="ISO_SYMBOL")
    private String isoSymbol;

    public String getName() {
        return name;
    }

    public Currency setName(String name) {
        this.name = name;
        return this;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public Currency setIsoCode(String isoCode) {
        this.isoCode = isoCode;
        return this;
    }

    public String getIsoSymbol() {
        return isoSymbol;
    }

    public Currency setIsoSymbol(String isoSymbol) {
        this.isoSymbol = isoSymbol;
        return this;
    }
                
    @Override
    protected Currency getThis() {
        return this;
    }   
}
