package com.samsunginc.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 * @author andrew jackson
 */
@Entity
@Audited
@Table(name = "_MEASUREMENT_INFORMATION")
public class MeasurementInformation extends DomainObject<MeasurementInformation> {
    @OneToOne(cascade = CascadeType.ALL)
    private Measurement exteriorHeight = new Measurement();
    @OneToOne(cascade = CascadeType.ALL)
    private Measurement exteriorWidth = new Measurement();
    @OneToOne(cascade = CascadeType.ALL)
    private Measurement exteriorLength = new Measurement();
    @OneToOne(cascade = CascadeType.ALL)
    private Measurement weight = new Measurement();

    public Measurement getExteriorHeight() {
        return exteriorHeight;
    }

    public MeasurementInformation setExteriorHeight(Measurement exteriorHeight) {
        this.exteriorHeight = exteriorHeight;
        return this;
    }

    public Measurement getExteriorWidth() {
        return exteriorWidth;
    }

    public MeasurementInformation setExteriorWidth(Measurement exteriorWidth) {
        this.exteriorWidth = exteriorWidth;
        return this;
    }

    public Measurement getExteriorLength() {
        return exteriorLength;
    }

    public MeasurementInformation setExteriorLength(Measurement exteriorLength) {
        this.exteriorLength = exteriorLength;
        return this;
    }

    public Measurement getWeight() {
        return weight;
    }

    public MeasurementInformation setWeight(Measurement weight) {
        this.weight = weight;
        return this;
    }

    @Override
    protected MeasurementInformation getThis() {
        return this;
    }
    
}
