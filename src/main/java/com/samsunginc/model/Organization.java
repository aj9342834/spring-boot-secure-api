package com.samsunginc.model;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author andrew jackson
 */
@Entity
@Audited
@Table(name = "_ORGANIZATION")
public class Organization extends DomainObject<Organization>{

    private String name;

    @OneToOne
    private Address address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    protected Organization getThis() {
        return this;
    }
}
