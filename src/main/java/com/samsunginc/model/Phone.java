package com.samsunginc.model;

import com.samsunginc.deserializer.PhoneSizeDeserializer;
import com.samsunginc.deserializer.PhoneTypeDeserializer;
import com.samsunginc.deserializer.UserDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Entity
@Audited
@Table(name = "_PHONE")
public class Phone extends DomainObject<Phone> {
    @Column(name = "SERIAL_NUMBER")
    private String serialNumber;
    @ManyToOne
    private User owner;
    @OneToOne(cascade = CascadeType.ALL)
    private MeasurementInformation measurementInformation;
    @ManyToOne 
    private PhoneType phoneType; 
    @ManyToOne 
    private PhoneSize phoneSize; 

    public String getSerialNumber() {
        return serialNumber;
    }

    public Phone setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
        return this;
    }

    public User getOwner() {
        return owner;
    }
    
    @JsonDeserialize(using = UserDeserializer.class)
    public Phone setOwner(User owner) {
        this.owner = owner;
        return this;
    }

    public MeasurementInformation getMeasurementInformation() {
        return measurementInformation;
    }

    public Phone setMeasurementInformation(MeasurementInformation measurementInformation) {
        this.measurementInformation = measurementInformation;
        return this;
    }

    /**
     * @return the phoneSize
     */
    public PhoneSize getPhoneSize() {
        return phoneSize;
    }

    /**
     * @param phoneSize the phoneSize to set
     * @return 
     */
    @JsonDeserialize(using = PhoneSizeDeserializer.class)
    public Phone setPhoneSize(PhoneSize phoneSize) {
        this.phoneSize = phoneSize;
        return this;
    }

    /**
     * @return the phoneType
     */
    public PhoneType getPhoneType() {
        return phoneType;
    }

    /**
     * @param phoneType the phoneType to set
     * @return 
     */
    @JsonDeserialize(using = PhoneTypeDeserializer.class)
    public Phone setPhoneType(PhoneType phoneType) {
        this.phoneType = phoneType;
        return this;
    }
    
    @Override
    protected Phone getThis() {
        return this;
    }
}
