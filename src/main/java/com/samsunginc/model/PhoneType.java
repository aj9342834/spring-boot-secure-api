/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samsunginc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author Andrew Jackkson <aj9342@gmail.com>
 */
@Entity
@Audited
@Table(name = "_PHONE_TYPE")
public class PhoneType extends DomainObject<PhoneType> {
    @Column(name="PHONE_TYPE")
    private String phoneTypeCode;
    @Column(name="DESCRIPTION")
    private String description;   

    /**
     * @return the phone_type_code
     */
    public String getPhoneTypeCode() {
        return phoneTypeCode;
    }

    /**
     * @param phoneTypeCode the phone_type_code to set
     * @return 
     */
    public PhoneType setPhoneTypeCode(String phoneTypeCode) {
        this.phoneTypeCode = phoneTypeCode;
        return this;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     * @return 
     */
    public PhoneType setDescription(String description) {
        this.description = description;
        return this;
    }
    
    @Override
    protected PhoneType getThis() {
        return this;
    } 
}
