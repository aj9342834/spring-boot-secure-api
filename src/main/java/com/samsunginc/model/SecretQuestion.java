package com.samsunginc.model;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author andrew jackson
 */
@Entity
@Audited
@Table(name = "_SECRET_QUESTION")
public class SecretQuestion extends DomainObject<SecretQuestion> {

    private String question;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    protected SecretQuestion getThis() {
        return this;
    }
}
