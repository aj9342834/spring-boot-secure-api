package com.samsunginc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.envers.Audited;

/**
 *
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Entity
@Audited
@Table(name = "_ADDRESS")
public class Address extends DomainObject<Address> {    
    @Column(name="STREET_ADDRESS")
    String streetAddress;
    @Column(name="STREET_ADDRESS_DETAILS")
    String streetAddressDetails;
    @Column(name="CITY")
    String city;
    @Column(name="PROVINCE")
    String province;
    @Column(name="COUNTRY_CODE")
    String countryCode;
    @Column(name="POSTAL_CODE")
    String postalCode;

    public String getStreetAddress() {
        return streetAddress;
    }

    public Address setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
        return this;
    }

    public String getStreetAddressDetails() {
        return streetAddressDetails;
    }

    public Address setStreetAddressDetails(String streetAddressDetails) {
        this.streetAddressDetails = streetAddressDetails;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Address setCity(String city) {
        this.city = city;
        return this;
    }

    public String getProvince() {
        return province;
    }

    public Address setProvince(String province) {
        this.province = province;
        return this;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public Address setCountryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public Address setPostalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }
    
    @Override
    protected Address getThis() {
        return this;
    }
}
