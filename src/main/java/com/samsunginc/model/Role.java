package com.samsunginc.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.envers.Audited;

/**
 *
 * @author andrew jackson
 */
@Entity
@Audited
@Table(name = "_ROLE")
public class Role extends DomainObject<Role> {
    @Column(name = "NAME")
    private String name;
    
    @ManyToMany(mappedBy = "roles")
    private List<User> users = new ArrayList<>();
    
    @ManyToMany
    @JoinTable(
        name = "_ROLE_PERMISSIONS", 
        joinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "UID"), 
        inverseJoinColumns = @JoinColumn(name = "PERMISSION_ID", referencedColumnName = "UID"))
    private List<Permission> permissions = new ArrayList<>();

    public Role() {
    }
    
    public Role(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public Role setName(String name) {
        this.name = name;
        return this;
    }

    @JsonIgnore
    @XmlTransient
    public List<User> getUsers() {
        return users;
    }

    public Role setUsers(List<User> users) {
        this.users = users;
        return this;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }
    
    @Override
    protected Role getThis() {
        return this;
    }
}
