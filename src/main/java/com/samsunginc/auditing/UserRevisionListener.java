package com.samsunginc.auditing;

import org.hibernate.envers.RevisionListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

/**
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
public class UserRevisionListener implements RevisionListener {    
    @Override
    public void newRevision(Object revisionEntity) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = "<unknown>";
        
        if (authentication != null && authentication.getPrincipal() instanceof User) {
            username = ((User) authentication.getPrincipal()).getUsername();
        }
        
        UserRevEntity revision = (UserRevEntity) revisionEntity;
        revision.setUsername(username);
    }
}
