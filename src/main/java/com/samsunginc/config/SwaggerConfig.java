/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samsunginc.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import static springfox.documentation.builders.PathSelectors.regex;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *
 * @author Andrew
 */
@EnableSwagger2
@Configuration
public class SwaggerConfig {
    @Value("${samsung.swagger.title}")
    private String title;
    @Value("${samsung.swagger.description}")
    private String description;
    @Value("${samsung.swagger.version}")
    private String version;
    @Value("${samsung.swagger.termsOfServiceUrl}")
    private String termsOfServiceUrl;
    @Value("${samsung.swagger.license}")
    private String license;
    @Value("${samsung.swagger.licenseUrl}")
    private String licenseUrl;
    @Value("${samsung.swagger.contact.name}")
    private String name;
    @Value("${samsung.swagger.contact.url}")
    private String url;
    @Value("${samsung.swagger.contact.email}")
    private String email;
    @Value("${samsung.api.version}")
    private String apiVersion;
      
    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.samsunginc"))
                .paths(regex(getApiBasePath()+".*"))
                .build()
                .apiInfo(metaInfo());
    }

    private ApiInfo metaInfo() {

        ApiInfo apiInfo = new ApiInfo(
                title,
                description,
                version,
                termsOfServiceUrl,
                new Contact(name, url,email),
                license,
                licenseUrl
        );
        
        return apiInfo;
    }

    private String getApiBasePath() {
        return "/api/" + apiVersion;
    }
    
}
