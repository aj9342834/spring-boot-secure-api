package com.samsunginc.config;

/**
 * Contains the Google reCAPTCHA pluging configuration defined at the application properties file
 *
 * @author Andrew Jackson <aj9342@gmail.com>
 */

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("google.recaptcha.key")
public class CaptchaSettings {
    private String site;
    private String secret;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
