package com.samsunginc.config;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.util.TimeZone;
import javax.validation.ConstraintValidatorFactory;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.hibernate.validator.HibernateValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.validation.beanvalidation.SpringConstraintValidatorFactory;
import org.springframework.web.client.RestTemplate;

/**
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
@EnableSpringConfigured
@Configuration
public class AppConfiguration {
    @Value("${samsung.default.time-zone}")
    private String defaultTimeZone;

    /**
     * This bean allows Spring to handle bean validation using the {@link HibernateValidator}.
     * Although this is Hibernates default validator, we manually declare this bean so that
     * dependency injection can occur during serialization/deserialization.
     * 
     * @param autowireCapableBeanFactory
     * @return {@link Validator}
     */
    @Bean
    public Validator validator(final AutowireCapableBeanFactory autowireCapableBeanFactory) {
        ConstraintValidatorFactory factory = new SpringConstraintValidatorFactory(autowireCapableBeanFactory);
        ValidatorFactory validatorFactory = Validation.byProvider(HibernateValidator.class)
                .configure()
                .constraintValidatorFactory(factory)
                .buildValidatorFactory();
        return validatorFactory.getValidator();
    }
    
    /**
     * This bean allows Spring to handle Jackson annotations and allows dependency injection.
     * This does not work without compile-time weaving using AspectJ (Spring AOP).
     * See Maven plugin "aspectj-maven-plugin" in POM for details.
     * @param context
     * @return {@link Jackson2ObjectMapperBuilder}
     */
    @Bean
    public Jackson2ObjectMapperBuilder jacksonBuilder(ApplicationContext context) {
        return new Jackson2ObjectMapperBuilder()
                .annotationIntrospector(new JacksonAnnotationIntrospector())
                .applicationContext(context)
                .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .featuresToDisable(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS)
                .timeZone(TimeZone.getTimeZone(defaultTimeZone != null ? defaultTimeZone : "America/Jamaica")); 
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
