package com.samsunginc.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.mapping.RepositoryDetectionStrategy;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.stereotype.Component;

/**
 * Provides configuration to spring-data-rest to make REST resources discoverable
 * 
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Component
public class SpringDataRestConfig extends RepositoryRestConfigurerAdapter  {    
    @Value("${samsung.api.version}")
    private String apiVersion;
    
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration rrc) {
        rrc.setBasePath(getApiBasePath());
        rrc.setRepositoryDetectionStrategy(RepositoryDetectionStrategy.RepositoryDetectionStrategies.ANNOTATED);
    }
    
    private String getApiBasePath() {
        return "/api/" + apiVersion;
    }
}
