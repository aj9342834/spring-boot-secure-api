package com.samsunginc.transport;

import java.util.ArrayList;
import java.util.List;

/**
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
public class ConstraintViolationsDto implements DataTransferObject {
    private List<ConstraintViolationDto> errors = new ArrayList<>();

    public ConstraintViolationsDto(List<ConstraintViolationDto> errors) {
        this.errors = errors;
    }

    public List<ConstraintViolationDto> getErrors() {
        return errors;
    }

    public ConstraintViolationsDto setErrors(List<ConstraintViolationDto> errors) {
        this.errors = errors;
        return this;
    }
}
