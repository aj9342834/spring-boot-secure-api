package com.samsunginc.transport;

import java.util.ArrayList;
import java.util.List;

/**
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
public class GeneralErrorsDto implements DataTransferObject {
    private List<GeneralErrorDto> errors = new ArrayList<>();

    public List<GeneralErrorDto> getErrors() {
        return errors;
    }

    public GeneralErrorsDto setErrors(List<GeneralErrorDto> errors) {
        this.errors = errors;
        return this;
    }

    public GeneralErrorsDto addError(GeneralErrorDto error) {
        this.errors.add(error);
        return this;
    }    
}
