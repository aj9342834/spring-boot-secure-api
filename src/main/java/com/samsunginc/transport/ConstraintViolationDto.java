package com.samsunginc.transport;

/**
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
public class ConstraintViolationDto implements DataTransferObject {
    private String fieldName;
    private String errorMessage;

    public String getFieldName() {
        return fieldName;
    }

    public ConstraintViolationDto setFieldName(String fieldName) {
        this.fieldName = fieldName;
        return this;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public ConstraintViolationDto setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }
}
