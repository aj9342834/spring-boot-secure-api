package com.samsunginc.transport;

/**
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
public class GeneralErrorDto {
    String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public GeneralErrorDto setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }
}
