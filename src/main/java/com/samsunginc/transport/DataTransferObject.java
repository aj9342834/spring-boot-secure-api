package com.samsunginc.transport;

import java.io.Serializable;

/**
 * @author  Andrew Jackson <aj9342@gmail.com>
 */
public interface DataTransferObject extends Serializable {
}
