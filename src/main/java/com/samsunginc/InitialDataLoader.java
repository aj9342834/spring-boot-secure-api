package com.samsunginc;

import com.samsunginc.repository.RoleRepository;
import com.samsunginc.repository.UserRepository;
//import com.samsunginc.repository.HolidayRepository;
import com.samsunginc.repository.CurrencyRepository;
import com.samsunginc.repository.PermissionRepository;
import com.samsunginc.repository.SecretQuestionRepository;
import com.samsunginc.repository.OrganizationRepository;
import com.samsunginc.repository.PhoneSizeRepository;
import com.samsunginc.repository.PhoneTypeRepository;
import com.samsunginc.repository.UnitOfMeasureRepository;
import com.samsunginc.repository.PhoneRepository;
import com.samsunginc.model.Organization;
import com.samsunginc.model.Phone;
import com.samsunginc.model.SecretQuestion;
import com.samsunginc.model.Role;
import com.samsunginc.model.User;
import com.samsunginc.model.PhoneSize;
import com.samsunginc.model.PhoneType;
import com.samsunginc.model.UserStatus;
import com.samsunginc.model.Currency;
import com.samsunginc.model.Permission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import org.springframework.context.annotation.Profile;

/**
 * @author andrew jackson
 */
@Component
@Profile({"dev", "test"})
public class InitialDataLoader {

    boolean alreadySetup = false;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PermissionRepository permissionRepository;
    @Autowired
    private UnitOfMeasureRepository unitOfMeasureRepository;
    @Autowired
    private CurrencyRepository currencyRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private PhoneRepository phoneRepository;
    @Autowired
    private SecretQuestionRepository secretQuestionRepository;
    @Autowired
    private OrganizationRepository organizationRepository;
    @Autowired
    private PhoneTypeRepository phoneTypeRepository;
    @Autowired
    private PhoneSizeRepository phoneSizeRepository;
//    @Autowired
//    private HolidayRepository holidayRepository;

    @EventListener
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {

        if (alreadySetup) {
            return;
        }
        Permission readPermission = createPermissionIfNotFound("READ_PRIVILEGE");
        Permission writePermission = createPermissionIfNotFound("WRITE_PRIVILEGE");
        List<Permission> adminPermissions = Arrays.asList(readPermission, writePermission);
        createRoleIfNotFound("ROLE_ADMIN", adminPermissions);
        createRoleIfNotFound("ROLE_USER", Arrays.asList(readPermission));

        // ORGANIZATIONS
        List<Organization> organizations = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            Organization o = new Organization();
            o.setName("Organization " + i);
            organizations.add(organizationRepository.saveAndFlush(o));

        }

        Role adminRole = roleRepository.findByName("ROLE_ADMIN");
        User user = new User();
        user.setUsername("tester");
        user.setPassword(passwordEncoder.encode("test"));
        user.setRoles(Arrays.asList(adminRole));
        user.setUserStatus(UserStatus.ACTIVE);
        user.setEmail("user@email.com");
        user.setOrganizations(organizations);
        userRepository.saveAndFlush(user);


        Currency bahamasdDollars = currencyRepository.saveAndFlush(new Currency()
                .setName("Bahamas Dollar")
                .setIsoCode("BSD")
                .setIsoSymbol("$"));

        Currency britishPound = currencyRepository.saveAndFlush(new Currency()
                .setName("United Kingdom Pound")
                .setIsoCode("GBP")
                .setIsoSymbol("£"));

        Currency unitedStatesDollars = currencyRepository.saveAndFlush(new Currency()
                .setName("United States Dollar")
                .setIsoCode("USD")
                .setIsoSymbol("$"));

        Currency jamaicanDollars = currencyRepository.saveAndFlush(new Currency()
                .setName("Jamaica Dollar")
                .setIsoCode("JMD")
                .setIsoSymbol("J$"));

        // SECRET QUESTIONS
        List<String> defaultSecretQuestions = Arrays.asList("What is your favourite colour?",
                "What was the name of your first pet?",
                "What is the name of the street you grew up on?",
                "What was the colour of your first car?",
                "What is the name of your elementary/primary school?",
                "What high school did you attend?",
                "What is your favourite movie?",
                "What is your father's middle name?",
                "What was your favourite place to visit as a child?",
                "In what city were you born?");
        for (String question : defaultSecretQuestions) {
            SecretQuestion secretQuestion = new SecretQuestion();
            secretQuestion.setQuestion(question);
            secretQuestionRepository.saveAndFlush(secretQuestion);
        }

        // Phone Type
        PhoneType phoneType = phoneTypeRepository.saveAndFlush(new PhoneType()
                .setPhoneTypeCode("dry")
                .setDescription("dry"));
        PhoneType phoneType1 = phoneTypeRepository.saveAndFlush(new PhoneType()
                .setPhoneTypeCode("reefer")
                .setDescription("reefer"));
        PhoneType phoneType2 = phoneTypeRepository.saveAndFlush(new PhoneType()
                .setPhoneTypeCode("open-top")
                .setDescription("open-top"));
        PhoneType phoneType3 = phoneTypeRepository.saveAndFlush(new PhoneType()
                .setPhoneTypeCode("flat-rack")
                .setDescription("flat-rack"));
        PhoneType phoneType4 = phoneTypeRepository.saveAndFlush(new PhoneType()
                .setPhoneTypeCode("high-cube-palletwide")
                .setDescription("high-cube-palletwide"));
        PhoneType phoneType5 = phoneTypeRepository.saveAndFlush(new PhoneType()
                .setPhoneTypeCode("tank")
                .setDescription("tank"));

        PhoneSize phoneSize0 = phoneSizeRepository.saveAndFlush(new PhoneSize()
                .setName("all"));
        PhoneSize phoneSize1 = phoneSizeRepository.saveAndFlush(new PhoneSize()
                .setName("20ft"));
        PhoneSize phoneSize2 = phoneSizeRepository.saveAndFlush(new PhoneSize()
                .setName("40ft"));
        PhoneSize phoneSize3 = phoneSizeRepository.saveAndFlush(new PhoneSize()
                .setName("40ft"));

        // CONTAINERS
        Phone phone1 = phoneRepository.saveAndFlush(new Phone()
                .setSerialNumber("C01_1")
                .setPhoneSize(phoneSize2)
                .setPhoneType(phoneType2));
        Phone phone2 = phoneRepository.saveAndFlush(new Phone()
                .setSerialNumber("C01_2")
                .setPhoneSize(phoneSize2)
                .setPhoneType(phoneType2));
        Phone phone3 = phoneRepository.saveAndFlush(new Phone()
                .setSerialNumber("C01_3")
                .setPhoneSize(phoneSize2)
                .setPhoneType(phoneType2));

        // Store a list of Jamaican Holiday's
        Calendar now = Calendar.getInstance();   // Gets the current date and time
        int currentYear = now.get(Calendar.YEAR);       // The current year

        alreadySetup = true;

    }

    @Transactional
    private Permission createPermissionIfNotFound(String name) {
        Permission permission = permissionRepository.findByName(name);
        if (permission == null) {
            permission = new Permission(name);
            permissionRepository.saveAndFlush(permission);
        }
        return permission;
    }

    @Transactional
    private Role createRoleIfNotFound(String name, List<Permission> permissions) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name);
            role.setPermissions(permissions);
            roleRepository.saveAndFlush(role);
        }
        return role;
    }
}
