package com.samsunginc.deserializer;

import com.samsunginc.model.Organization;
import com.samsunginc.model.SecretQuestion;
import com.samsunginc.service.OrganizationService;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import java.io.IOException;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Configurable(autowire = Autowire.BY_TYPE)
public class OrganizationDeserializer extends StdDeserializer<Organization> {

    @Autowired
    private OrganizationService organizationService;

    public OrganizationDeserializer() {
        this(SecretQuestion.class);
    }

    protected OrganizationDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Organization deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String uid = jsonParser.getText();
        return (uid != null && !uid.isEmpty() && uid.matches("^\\d+$")) ?
                organizationService.findOne(Long.parseLong(uid)) : jsonParser.readValueAs(Organization.class);
    }
}
