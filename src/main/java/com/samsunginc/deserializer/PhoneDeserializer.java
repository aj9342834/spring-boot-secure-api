package com.samsunginc.deserializer;

import com.samsunginc.model.Phone;
import com.samsunginc.service.PhoneService;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Configurable(autowire = Autowire.BY_TYPE)
public class PhoneDeserializer extends StdDeserializer<Phone> {
    @Autowired
    private PhoneService containerService;

    public PhoneDeserializer() {
        this(Phone.class);
    }
    
    private PhoneDeserializer(Class<?> vc) {
        super(vc);
    }
    
    @Override
    public Phone deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String uid = p.getText();
        return (uid != null && !uid.isEmpty() && uid.matches("^\\d+$")) ? 
                containerService.findById(Long.parseLong(uid)).get() : p.readValueAs(Phone.class);
    }

    @Override
    public Class<?> handledType() {
        return Phone.class;
    }
}
