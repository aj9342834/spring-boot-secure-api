package com.samsunginc.deserializer;

import com.samsunginc.model.Currency;
import com.samsunginc.service.CurrencyService;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Configurable(autowire = Autowire.BY_TYPE)
public class CurrencyDeserializer extends StdDeserializer<Currency> {
    @Autowired
    private CurrencyService currencyService;

    public CurrencyDeserializer() {
        this(Currency.class);
    }
    
    private CurrencyDeserializer(Class<?> vc) {
        super(vc);
    }
    
    @Override
    public Currency deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String uid = p.getText();
        return (uid != null && !uid.isEmpty() && uid.matches("^\\d+$")) ? 
                currencyService.findOne(Long.parseLong(uid)) : p.readValueAs(Currency.class);
    }

    @Override
    public Class<?> handledType() {
        return Currency.class;
    }
}
