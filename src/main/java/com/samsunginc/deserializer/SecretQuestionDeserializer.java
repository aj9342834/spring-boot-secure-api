package com.samsunginc.deserializer;

import com.samsunginc.model.SecretQuestion;
import com.samsunginc.service.SecretQuestionService;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import java.io.IOException;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Configurable(autowire = Autowire.BY_TYPE)
public class SecretQuestionDeserializer extends StdDeserializer<SecretQuestion> {

    @Autowired
    private SecretQuestionService secretQuestionService;

    public SecretQuestionDeserializer() {
        this(SecretQuestion.class);
    }

    protected SecretQuestionDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public SecretQuestion deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String uid = jsonParser.getText();
        return (uid != null && !uid.isEmpty() && uid.matches("^\\d+$")) ?
                secretQuestionService.findOne(Long.parseLong(uid)) : jsonParser.readValueAs(SecretQuestion.class);

    }
}