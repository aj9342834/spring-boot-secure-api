package com.samsunginc.deserializer;

import com.samsunginc.model.User;
import com.samsunginc.service.UserService;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * @author andrew jackson
 */
@Configurable(autowire = Autowire.BY_TYPE)
public class UserDeserializer extends StdDeserializer<User> {
    @Autowired
    private UserService partyService;

    public UserDeserializer() {
        this(User.class);
    }
    
    private UserDeserializer(Class<?> vc) {
        super(vc);
    }
    
    @Override
    public User deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String uid = p.getText();
        return (uid != null && !uid.isEmpty() && uid.matches("^\\d+$")) ? 
                partyService.findOne(Long.parseLong(uid)) : p.readValueAs(User.class);
    }

    @Override
    public Class<?> handledType() {
        return User.class;
    }
}
