package com.samsunginc.deserializer;

import com.samsunginc.model.PhoneSize;
import com.samsunginc.service.PhoneSizeService;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Configurable(autowire = Autowire.BY_TYPE)
public class PhoneSizeDeserializer extends StdDeserializer<PhoneSize> {
    @Autowired
    private PhoneSizeService currencyService;

    public PhoneSizeDeserializer() {
        this(PhoneSize.class);
    }
    
    private PhoneSizeDeserializer(Class<?> vc) {
        super(vc);
    }
    
    @Override
    public PhoneSize deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String uid = p.getText();
        return (uid != null && !uid.isEmpty() && uid.matches("^\\d+$")) ? 
                currencyService.findOne(Long.parseLong(uid)) : p.readValueAs(PhoneSize.class);
    }

    @Override
    public Class<?> handledType() {
        return PhoneSize.class;
    }
}
