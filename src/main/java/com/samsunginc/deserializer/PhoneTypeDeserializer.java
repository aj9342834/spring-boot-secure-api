
package com.samsunginc.deserializer;
import com.samsunginc.model.PhoneType;
import com.samsunginc.service.PhoneTypeService;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 *
 * @author Andrew
 */
@Configurable(autowire = Autowire.BY_TYPE)
public class PhoneTypeDeserializer extends StdDeserializer<PhoneType>{
    @Autowired
    private PhoneTypeService containerTypeService;

    public PhoneTypeDeserializer() {
        this(PhoneType.class);
    }
    
    private PhoneTypeDeserializer(Class<?> vc) {
        super(vc);
    }
    
    @Override
    public PhoneType deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String uid = p.getText();
        return (uid != null && !uid.isEmpty() && uid.matches("^\\d+$")) ? 
                containerTypeService.findOne(Long.parseLong(uid)) : p.readValueAs(PhoneType.class);
    }

    @Override
    public Class<?> handledType() {
        return PhoneType.class;
   
    }
}
