package com.samsunginc.deserializer;

import com.samsunginc.model.Organization;
import com.samsunginc.model.SecretQuestion;
import com.samsunginc.service.OrganizationService;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Configurable(autowire = Autowire.BY_TYPE)
public class OrganizationListDeserializer extends StdDeserializer<List<Organization>> {

    @Autowired
    private OrganizationService organizationService;

    public OrganizationListDeserializer() {
        this(SecretQuestion.class);
    }

    protected OrganizationListDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public List<Organization> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        List<Organization> organizations = new ArrayList<>();
        if (JsonToken.START_ARRAY == jsonParser.getCurrentToken()) {
            while (JsonToken.END_ARRAY != jsonParser.nextToken()) {
                String uid = jsonParser.getText();
                if (uid != null && !uid.isEmpty() && uid.matches("^\\d+$")){
                    organizations.add(organizationService.findOne(Long.parseLong(uid)));
                } else {
                    organizations.add(jsonParser.readValueAs(Organization.class));
                }
            }
        }

        return organizations;
    }
}
