package com.samsunginc.deserializer;

import com.samsunginc.model.UnitOfMeasure;
import com.samsunginc.service.UnitOfMeasureService;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@Configurable(autowire = Autowire.BY_TYPE)
public class UnitOfMeasureDeserializer extends StdDeserializer<UnitOfMeasure> {
    @Autowired
    private UnitOfMeasureService unitOfMeasureService;

    public UnitOfMeasureDeserializer() {
        this(UnitOfMeasure.class);
    }
    
    private UnitOfMeasureDeserializer(Class<?> vc) {
        super(vc);
    }
    
    @Override
    public UnitOfMeasure deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String uid = p.getText();
        return (uid != null && !uid.isEmpty() && uid.matches("^\\d+$")) ? 
                unitOfMeasureService.findOne(Long.parseLong(uid)) : p.readValueAs(UnitOfMeasure.class);
    }

    @Override
    public Class<?> handledType() {
        return UnitOfMeasure.class;
    }
}
