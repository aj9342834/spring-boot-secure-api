package com.samsunginc.controller;

import com.samsunginc.model.SecurityLevelConfiguration;
import com.samsunginc.service.SecurityLevelConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest interface for {@link SecurityLevelConfiguration}
 * @author Andrew Jackson <aj9342@gmail.com>
 */

@RestController
@RequestMapping("/api/${samsung.api.version}/security-level-configuration")
public class SecurityLevelConfigurationController {

    @Autowired
    SecurityLevelConfigurationService securityLevelConfigurationService;

    /**
     *
     * @return the system's {@link SecurityLevelConfiguration} object
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<SecurityLevelConfiguration> getSecurityConfiguration() {
        SecurityLevelConfiguration securityLevelConfiguration = securityLevelConfigurationService.getSecurityConfiguration();
        return new ResponseEntity<>(securityLevelConfiguration, HttpStatus.OK);
    }

    /**
     * Updates the system's {@link SecurityLevelConfiguration} object
     * @param securityLevelConfiguration
     * @return the updated system's Security Configuration object
     */
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<SecurityLevelConfiguration> updateSecurityConfiguration(@RequestBody SecurityLevelConfiguration securityLevelConfiguration) {
        SecurityLevelConfiguration updatedSecurityLevelConfiguration = securityLevelConfigurationService.updateSecurityConfiguration(securityLevelConfiguration);
        return new ResponseEntity<>(updatedSecurityLevelConfiguration, HttpStatus.OK);
    }
}
