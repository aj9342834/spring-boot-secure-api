package com.samsunginc.controller;

import com.samsunginc.model.Organization;
import com.samsunginc.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Rest interface for {@link List} of {@link Organization}
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@RestController
@RequestMapping("/api/${samsung.api.version}/organizations")
public class OrganizationsController {

    @Autowired
    private OrganizationService organizationService;

    /**
     * Rest method for listing the {@link Organization}
     * @return a {@link ResponseEntity} object with a {@link List} of {@link Organization} as {@code body}
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Organization>> list() {
        return new ResponseEntity<>(organizationService.list(), HttpStatus.OK);
    }
}
