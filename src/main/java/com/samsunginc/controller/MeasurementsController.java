package com.samsunginc.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.samsunginc.model.Measurement;
import com.samsunginc.service.MeasurementService;

@RestController
@RequestMapping("/api/${samsung.api.version}/measurements")
public class MeasurementsController {

    @Autowired
    MeasurementService measurementService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Measurement>> list() throws JsonProcessingException {        
        List<Measurement> measurements = measurementService.list();
        
        return new ResponseEntity<>(measurements, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Measurement> add(@RequestBody Measurement measurement) {
        Measurement savedMeasurement = measurementService.add(measurement);

        if (savedMeasurement == null) {
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        } else {
            return new ResponseEntity<>(savedMeasurement, HttpStatus.OK);
        }
    }
}
