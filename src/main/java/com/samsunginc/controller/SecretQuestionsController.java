package com.samsunginc.controller;

import com.samsunginc.model.SecretQuestion;
import com.samsunginc.service.SecretQuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Rest interface for {@link List} of {@link SecretQuestion}
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@RestController
@RequestMapping("/api/${samsung.api.version}/secret-questions")
public class SecretQuestionsController {

    @Autowired
    private SecretQuestionService secretQuestionService;

    /**
     * Rest method for listing the {@link SecretQuestion}
     * @return a {@link ResponseEntity} object with a {@link java.util.List} of {@link SecretQuestion} as {@code body}
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<SecretQuestion>> list() {
        return new ResponseEntity<>(secretQuestionService.list(), HttpStatus.OK);
    }
}
