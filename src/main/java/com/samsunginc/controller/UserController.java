package com.samsunginc.controller;


import com.samsunginc.model.UserStatus;
import com.samsunginc.validator.UserRegistrationValidator;
import com.samsunginc.validator.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import com.samsunginc.model.User;
import com.samsunginc.service.UserService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/${samsung.api.version}/user/{id:[\\d]+}")
public class UserController {

    @Autowired
    UserService userService;

    @InitBinder("userStatusHolder")
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(new UserStatusHolderValidator());
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<User> get(@PathVariable("id") Long id) {
        User user = userService.findOne(id);

        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<User> update(@PathVariable("id") Long id, @RequestBody User user) {

        User updatedUser = userService.update(id, user);

        if (updatedUser == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(updatedUser, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        userService.delete(id);
    }

    @RequestMapping(path = "userStatus", method = RequestMethod.POST)
    public ResponseEntity<User> updateUserStatus(@PathVariable("id") Long id, @Valid @RequestBody UserStatusHolder statusHolder) {
        if (userService.findOne(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        UserStatus userStatus = UserStatus.valueOf(statusHolder.getUserStatus());
        User updatedUser = userService.updateUserStatus(id, userStatus);
        return new ResponseEntity<>(updatedUser, HttpStatus.OK);
    }

    public static class UserStatusHolder {
        private String userStatus;

        public String getUserStatus() {
            return userStatus;
        }

        public void setUserStatus(String userStatus) {
            this.userStatus = userStatus;
        }
    }

    public static class UserStatusHolderValidator implements Validator{

        @Override
        public boolean supports(Class<?> aClass) {
            return UserStatusHolder.class.isAssignableFrom(aClass);
        }

        @Override
        public void validate(Object o, Errors errors) {
            if (o == null || ((UserStatusHolder) o).getUserStatus() == null) {
                errors.rejectValue("userStatus", "Required");
                throw new ValidationException(errors.getAllErrors());
            }
            try {
                UserStatus.valueOf(((UserStatusHolder) o).getUserStatus());
            } catch (IllegalArgumentException e) {
                errors.rejectValue("userStatus", "Invalid");
                throw new ValidationException(errors.getAllErrors());
            }
        }
    }
}
