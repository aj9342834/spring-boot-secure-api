package com.samsunginc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.samsunginc.model.Measurement;
import com.samsunginc.service.MeasurementService;

@RestController
@RequestMapping("/api/${samsung.api.version}/measurement/{id:[\\d]+}")
public class MeasurementController {

    @Autowired
    MeasurementService measurementService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Measurement> get(@PathVariable("id") Long id) {
        Measurement measurement = measurementService.findOne(id);

        if (measurement == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(measurement, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Measurement> update(@PathVariable("id") Long id, @RequestBody Measurement measurement) {

        Measurement updatedMeasurement = measurementService.update(id, measurement);

        if (updatedMeasurement == null) {
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        } else {
            return new ResponseEntity<>(updatedMeasurement, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        measurementService.delete(id);
    }
}
