
package com.samsunginc.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.samsunginc.model.PhoneSize;
import com.samsunginc.service.PhoneSizeService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Andrew Jackson
 */
@RestController
@RequestMapping("/api/${samsung.api.version}/phone-sizes")
public class PhoneSizesController {
        @Autowired
    PhoneSizeService phoneSizeService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<PhoneSize>> get() throws JsonProcessingException {
        List<PhoneSize> phoneSizes = phoneSizeService.list();

        if (phoneSizes == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(phoneSizes, HttpStatus.OK);
        }
    }
}
