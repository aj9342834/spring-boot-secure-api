package com.samsunginc.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.samsunginc.model.UnitOfMeasure;
import com.samsunginc.service.UnitOfMeasureService;

@RestController
@RequestMapping("/api/${samsung.api.version}/units-of-measure")
public class UnitsOfMeasureController {

    @Autowired
    UnitOfMeasureService unitOfMeasureService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<UnitOfMeasure>> list() throws JsonProcessingException {        
        List<UnitOfMeasure> unitsOfMeasure = unitOfMeasureService.list();
        
        return new ResponseEntity<>(unitsOfMeasure, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<UnitOfMeasure> add(@RequestBody UnitOfMeasure unitOfMeasure) {
        UnitOfMeasure savedUnitOfMeasure = unitOfMeasureService.add(unitOfMeasure);

        if (savedUnitOfMeasure == null) {
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        } else {
            return new ResponseEntity<>(savedUnitOfMeasure, HttpStatus.OK);
        }
    }
}
