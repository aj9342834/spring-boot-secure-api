package com.samsunginc.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.samsunginc.model.Phone;
import com.samsunginc.service.PhoneService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;

/**
 * @author Andrew Jackson
 */
@RestController
@RequestMapping("/api/${samsung.api.version}/phones")
public class PhonesController {

    @Autowired
    PhoneService phoneService;

    /**
     *
     * @return Phone - List
     * @throws JsonProcessingException
     */
    @ApiOperation(notes = "Returns a list of phones",
            value = "Return phone list",
            nickname = "list",
            tags = {"Phones"})
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Phone>> list() throws JsonProcessingException {
        List<Phone> phones = phoneService.list();
        //Cache list
        return ResponseEntity.ok()
                             .cacheControl(CacheControl.maxAge(3000, TimeUnit.SECONDS)
                             .cachePublic())
                             .body(phones);
    }

    /**
     *
     * @param phone
     * @return Phone - Object
     * @throws java.lang.Exception
     */
     @ApiOperation(notes = "Add a new phone",
            value = "Add phone",
            nickname = "add",
            tags = {"Phones"})
     @ApiResponses({
        @ApiResponse(code = 200, message = "Phone was added!", response = Phone.class),
        @ApiResponse(code = 400, message = "Bad Request", response = Phone.class),
        @ApiResponse(code = 404, message = "Phone was not saved!", response = Phone.class)
     })
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Phone> add(@RequestBody Phone phone) throws Exception {
        Optional<Phone> savedPhone = phoneService.add(phone);

        if (savedPhone.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        }
        return new ResponseEntity<>(savedPhone.get(), HttpStatus.OK);
    }
}
