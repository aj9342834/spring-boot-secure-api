package com.samsunginc.controller;

import com.samsunginc.service.SecretQuestionService;
import com.samsunginc.service.ReCaptchaValidationService;
import com.samsunginc.service.UserNotificationService;
import com.samsunginc.service.OrganizationService;
import com.samsunginc.service.UserService;
import com.samsunginc.service.UserRegistrationValidationService;
import com.samsunginc.service.SecurityLevelConfigurationService;
import com.samsunginc.model.Organization;
import com.samsunginc.model.SecurityLevelConfiguration;
import com.samsunginc.model.SecretQuestion;
import com.samsunginc.model.User;
import com.samsunginc.model.UserStatus;
import com.samsunginc.validator.UserRegistrationValidator;
import com.samsunginc.validator.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@RestController
@RequestMapping("api/${samsung.api.version}/user-registration")
public class UserRegistrationController {

    @Autowired
    UserRegistrationValidationService validationService;

    @Autowired
    UserService userService;

    @Autowired
    private UserNotificationService notificationService;

    @Autowired
    private ReCaptchaValidationService reCaptchaValidationService;

    @Autowired
    private SecurityLevelConfigurationService securityLevelConfigurationService;

    @Autowired
    private OrganizationService organizationService;

    @Autowired
    private SecretQuestionService secretQuestionService;

    @InitBinder("userRegistration")
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(new UserRegistrationValidator(validationService, reCaptchaValidationService));
    }

    @RequestMapping(path = "security-level-configuration", method = RequestMethod.GET)
    public ResponseEntity<SecurityLevelConfiguration> getSecurityConfiguration() {
        SecurityLevelConfiguration securityLevelConfiguration = securityLevelConfigurationService.getSecurityConfiguration();
        return new ResponseEntity<>(securityLevelConfiguration, HttpStatus.OK);
    }

    @RequestMapping(path = "organizations", method = RequestMethod.GET)
    public ResponseEntity<List<Organization>> listOrganizations() {
        return new ResponseEntity<>(organizationService.list(), HttpStatus.OK);
    }

    @RequestMapping(path = "secret-questions", method = RequestMethod.GET)
    public ResponseEntity<List<SecretQuestion>> listSecretQuestions() {
        return new ResponseEntity<>(secretQuestionService.list(), HttpStatus.OK);
    }

    @RequestMapping(path = "validate", method = RequestMethod.POST)
    public ResponseEntity<User> validateUserRegistrationField(@RequestBody User user, @RequestParam(name = "target", required = false) String target) {
        List<UserRegistrationValidationService.HasFieldError> validationResults = new ArrayList<>();
        target = target != null ? target : "";
        switch (target) {
            case "username" :
                validationResults.add(validationService.validateUsername(user));
                break;
            case "email" :
                validationResults.add(validationService.validateUserEmail(user));
                break;
            case "password" :
                validationResults.add(validationService.validateUserPassword(user));
                break;
            default:
                validationResults.add(validationService.validateUsername(user));
                validationResults.add(validationService.validateUserEmail(user));
                validationResults.add(validationService.validateUserPassword(user));
        }
        List<ObjectError> errors = new ArrayList<>();
        for (UserRegistrationValidationService.HasFieldError fieldError : validationResults) {
            for (String e : fieldError.getErrors()) {
                errors.add(new FieldError("User", fieldError.getField(), e));
            }
        }
        if (!errors.isEmpty()) {
            throw new ValidationException(errors);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<User> add(@Valid @RequestBody UserRegistration userRegistration) {
        userRegistration.getUser().setUserStatus(UserStatus.PENDING);
        User addedUser = userService.add(userRegistration.getUser());
        notificationService.sendNewUserRequestNotification(addedUser);
        if (addedUser == null) {
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        } else {
            return new ResponseEntity<>(addedUser, HttpStatus.OK);
        }
    }

    public static class UserRegistration {
        private User user;
        private String captchaResponse;

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public String getCaptchaResponse() {
            return captchaResponse;
        }

        public void setCaptchaResponse(String captchaResponse) {
            this.captchaResponse = captchaResponse;
        }
    }

}
