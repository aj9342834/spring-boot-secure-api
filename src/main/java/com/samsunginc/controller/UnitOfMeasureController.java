package com.samsunginc.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.samsunginc.model.UnitOfMeasure;
import com.samsunginc.service.UnitOfMeasureService;

@RestController
@RequestMapping("/api/${samsung.api.version}/unit-of-measure/{id:[\\d]+}")
public class UnitOfMeasureController {

    @Autowired
    UnitOfMeasureService unitOfMeasureService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<UnitOfMeasure> get(@PathVariable("id") Long id) {
        UnitOfMeasure unitOfMeasure = unitOfMeasureService.findOne(id);

        if (unitOfMeasure == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(unitOfMeasure, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<UnitOfMeasure> update(@PathVariable("id") Long id, @RequestBody UnitOfMeasure unitOfMeasure) {

        UnitOfMeasure updatedUnitOfMeasure = unitOfMeasureService.update(id, unitOfMeasure);

        if (updatedUnitOfMeasure == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(updatedUnitOfMeasure, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) {
        unitOfMeasureService.delete(id);
    }
}
