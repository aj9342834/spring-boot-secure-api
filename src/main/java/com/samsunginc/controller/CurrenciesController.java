package com.samsunginc.controller;

import com.samsunginc.model.Currency;
import com.samsunginc.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



import java.util.List;

@RestController
@RequestMapping("/api/${samsung.api.version}/currencies")
public class CurrenciesController {

    @Autowired
    CurrencyService currencyService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Currency>> get() throws  Exception {
        List<Currency> currencys = currencyService.list();

        if (currencys == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(currencys, HttpStatus.OK);
        }
    }
    
    

}
