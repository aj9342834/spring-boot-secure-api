package com.samsunginc.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.samsunginc.model.User;
import com.samsunginc.service.UserService;

@RestController
@RequestMapping("/api/${samsung.api.version}/users")
public class UsersController {

    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<User>> list() throws JsonProcessingException {        
        List<User> users = userService.list();
        
        if (users.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(users, HttpStatus.OK);
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<User> add(@RequestBody User user) {
        User savedUser = userService.add(user);

        if (savedUser == null) {
            return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
        } else {
            return new ResponseEntity<>(savedUser, HttpStatus.OK);
        }
    }

    @RequestMapping(path = "userStatus/toggle", method = RequestMethod.POST)
    public ResponseEntity<Void> toggleUserStatus(@RequestBody Set<Long> usersUid) {
        for (Long uid : usersUid) {
            userService.toggleUserStatus(uid);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
