package com.samsunginc.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.samsunginc.model.MeasurementInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.samsunginc.model.Phone;
import com.samsunginc.service.PhoneService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Optional;

@RestController
@RequestMapping("/api/${samsung.api.version}/phone/{id:[\\d]+}")
public class PhoneController {

    @Autowired
    PhoneService phoneService;

    /**
     *
     * @param id
     * @return
     * @throws JsonProcessingException
     */
    @ApiOperation(notes = "Returns a phone object when ID is found",
            value = "Find phone by ID",
            nickname = "get",
            tags = {"Phone"})
     @ApiResponses({
        @ApiResponse(code = 200, message = "Phone was found!", response = Phone.class),
        @ApiResponse(code = 400, message = "Bad Request", response = Phone.class),
        @ApiResponse(code = 404, message = "Phone was not found!", response = Phone.class)
     })
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Phone> get(@PathVariable("id") Long id) throws JsonProcessingException {
        Optional<Phone> phone = phoneService.findById(id);

        if (!phone.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(phone.get(), HttpStatus.OK);
    }

    /**
     *
     * @param id
     * @param phone
     * @return
     * @throws java.lang.Exception
     */
    @ApiOperation(notes = "Update phone when ID is found",
            value = "Update phone by ID",
            nickname = "update",
            tags = {"Phone"})
    @ApiResponses({
        @ApiResponse(code = 200, message = "Phone was updated!", response = Phone.class),
        @ApiResponse(code = 400, message = "Bad Request", response = Phone.class),
        @ApiResponse(code = 404, message = "Phone was not updated!", response = Phone.class)
     })
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Phone> update(@PathVariable("id") Long id, @RequestBody Phone phone) throws Exception {

        Optional<Phone> updatedPhone = phoneService.update(id, phone);

        if (!updatedPhone.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(updatedPhone.get(), HttpStatus.OK);
    }

    @ApiOperation(notes = "Delete phone by ID",
            value = "Delete phone by ID",
            nickname = "update",
            tags = {"Phone"})
    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") Long id) throws Exception {
        phoneService.delete(id);
    }

    /**
     *
     * @param id
     * @return
     * @throws JsonProcessingException
     */
    @ApiOperation(notes = "Returns phone measurement information from phone ID",
            value = "Find measurement-information by phone ID",
            nickname = "getPhoneMeasurementInformation",
            tags = {"Phone"})
    @ApiResponses({
        @ApiResponse(code = 200, message = "Phone measurement information Found!", response = MeasurementInformation.class),
        @ApiResponse(code = 400, message = "Bad Request", response = MeasurementInformation.class),
        @ApiResponse(code = 404, message = "Phone was not found!", response = MeasurementInformation.class)
     })
    @RequestMapping(value = "measurement-information", method = RequestMethod.GET)
    public ResponseEntity<MeasurementInformation> getPhoneMeasurementInformation(@PathVariable("id") Long id) throws JsonProcessingException {
        Optional<Phone> phone = phoneService.findById(id);

        if (!phone.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(phone.get().getMeasurementInformation(), HttpStatus.OK);
    }

}
