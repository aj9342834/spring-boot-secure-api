package com.samsunginc.controller;

import com.samsunginc.model.PhoneType;
import com.samsunginc.service.PhoneTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Andrew Jackson
 */
import java.util.List;

@RestController
@RequestMapping("/api/${samsung.api.version}/phone-types")
public class PhoneTypesController {
    @Autowired
    PhoneTypeService phoneTypeService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<PhoneType>> get() {
        List<PhoneType> phoneTypes = phoneTypeService.list();

        if (phoneTypes == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(phoneTypes, HttpStatus.OK);
        }
    }
    
}
