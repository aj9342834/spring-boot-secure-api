INSERT INTO _user(
            uid, email, first_name,
            last_name, middle_initial, secret_question_answer,
            user_status, username, secret_question_uid)
    VALUES (999, 'john_doe@fakemail.com', 'John',
            'Doe', 'S', 'Beethoven',
            'PENDING', 'johnDoe', 2);
