INSERT INTO _role(
            uid, uuid, name)
    VALUES (999, 'uid-asdf-asdf', 'ROLE_ADMIN');

INSERT INTO _user(
            uid, email, first_name, last_name, middle_initial,
            secret_question_answer, user_status,
            username, secret_question_uid)
    VALUES (999, 'super-admin@samsunginc.com', 'Fake', 'User', 'M',
            'Answer', 'ACTIVE',
            'admin', 1);

INSERT INTO _user_roles(
            user_id, role_id)
    VALUES (999, 999);
