package com.samsunginc.service;

import com.samsunginc.service.SecretQuestionService;
import com.samsunginc.model.SecretQuestion;
import com.samsunginc.repository.SecretQuestionRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Test class for {@link SecretQuestionService} methods
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public class SecretQuestionServiceTest {

    @Mock
    private SecretQuestionRepository secretQuestionRepository;

    @InjectMocks
    private SecretQuestionService secretQuestionService;

    @Before
    public void init() {
        initMocks(this);
    }

    /**
     * Test retrieval of list of {@link SecretQuestion} objects
     */
    @Test
    public void testRetrievalOfAllSecretQuestions() {
        when(secretQuestionRepository.findAll()).thenReturn(buildSecretQuestionMockList(3));
        List<SecretQuestion> secretQuestions = secretQuestionService.list();
        assertEquals(3, secretQuestions.size());
        assertEquals(Long.valueOf(2), secretQuestions.get(2).getUid());
    }

    private ArrayList<SecretQuestion> buildSecretQuestionMockList(int amount) {
        ArrayList<SecretQuestion> secretQuestions = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            SecretQuestion secretQuestion = new SecretQuestion();
            secretQuestion.setUid(Long.valueOf(i));
            secretQuestion.setQuestion("Question: " + i);
            secretQuestions.add(secretQuestion);
        }
        return secretQuestions;
    }

    /**
     * Test retrieval of a single {@link SecretQuestion}
     */
    @Test
    public void testGetOfSecretQuestionByUid() {
        SecretQuestion secretQuestion = new SecretQuestion();
        String question = "secret";
        secretQuestion.setQuestion(question);
        when(secretQuestionRepository.findOne(1L)).thenReturn(secretQuestion);
        SecretQuestion foundSecretQuestion = secretQuestionService.findOne(1L);
        assertEquals(question, foundSecretQuestion.getQuestion());
    }
}
