package com.samsunginc.service;

import com.samsunginc.service.UserNotificationService;
import com.samsunginc.model.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.subethamail.wiser.Wiser;

import static com.samsunginc.service.WiserAssertions.assertReceivedMessage;

/**
 * Integration Test class for {@link UserNotificationService}
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles("integration")
public class UserNotificationServiceIT {

    private Wiser wiser;

    @Autowired
    UserNotificationService notificationService;

    @Value("${samsung.mail.defaultsender.address}")
    private String defaultSystemSender;


    @Before
    public void setup() {
        wiser = new Wiser();
        wiser.start();
    }

    @After
    public void tearDown() throws Exception {
        wiser.stop();
    }

    /**
     * Test that a mail is correctly send for a new User Request.
     * @throws Exception
     */
    @Test
    @Sql("create_admin_user.sql")
    public void testSendNewUserRequestNotification() throws Exception {
        User user = createJohnDoeUser();
        notificationService.sendNewUserRequestNotification(user);
        assertReceivedMessage(wiser)
                .from(defaultSystemSender)
                .to("super-admin@samsunginc.com")
                .withSubject("New User Account Request")
                .withMimeContentContaining("John Doe has submitted a request for User Account");
    }

    private User createJohnDoeUser() {
        User user = new User();
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setEmail("johndoe@fakemail.com");
        return user;
    }

    /**
     * Test that a mail is correctly send for an User approval
     */
    @Test
    public void testSendUserApprovalNotification() throws Exception {
        User user = createJohnDoeUser();
        notificationService.sendUserActivationNotification(user);
        assertReceivedMessage(wiser)
                .from(defaultSystemSender)
                .to(user.getEmail())
                .withSubject("User Account Activated")
                .withMimeContentContaining("Dear John Doe")
                .withMimeContentContaining("Your Samsung Agent user account has been successfully activated");
    }

    /**
     * Test that a mail is correctly send for an User rejection
     */
    @Test
    public void testSendUserRejectionNotification() throws Exception {
        User user = createJohnDoeUser();
        notificationService.sendUserRejectionNotification(user, "Access Denied");
        assertReceivedMessage(wiser)
                .from(defaultSystemSender)
                .to(user.getEmail())
                .withSubject("User Account Request Rejected")
                .withMimeContentContaining("Dear John Doe")
                .withMimeContentContaining("Your Samsung Agent user account request has been rejected for the following reason - Access Denied");
    }

    /**
     * Test that a mail is correctly send for an User approval
     */
    @Test
    public void testSendUserApprovalNotificationWithTextAndHtmlFormats() throws Exception {
        User user = createJohnDoeUser();
        notificationService.sendUserActivationNotification(user);
        assertReceivedMessage(wiser)
                .from(defaultSystemSender)
                .to(user.getEmail())
                .withSubject("User Account Activated")
                .withMimeContentContaining("Dear John Doe")
                .withMimeContentContaining("Your Samsung Agent user account has been successfully activated")
                .withMimeContentContaining("Dear John Doe :: Your Samsung Agent user account has been successfully activated");
    }
}