package com.samsunginc.service;

import com.samsunginc.service.SecurityLevelConfigurationService;
import com.samsunginc.model.SecurityLevelConfiguration;
import com.samsunginc.repository.SecurityLevelConfigurationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Integration test class for {@link SecurityLevelConfigurationService}
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
@ActiveProfiles("integration")
public class SecurityLevelConfigurationServiceIT {

    @Autowired
    private SecurityLevelConfigurationRepository securityLevelConfigurationRepository;

    @Autowired
    private SecurityLevelConfigurationService securityLevelConfigurationService;

    @Before
    public void init() {
        initMocks(this);
    }

    /**
     * Test retrieval of default {@link SecurityLevelConfiguration} object
     */
    @Test
    public void testRetrievalOfSecurityConfiguration() {
        SecurityLevelConfiguration foundSecurityLevelConfiguration = securityLevelConfigurationService.getSecurityConfiguration();
        assertNotNull("Found existent SecurityLevelConfiguration", foundSecurityLevelConfiguration);
    }

    /**
     * Test retrieval of default {@link SecurityLevelConfiguration} object even when it does not exist
     */
    @Test
    @Sql("delete_configurations.sql")
    public void testRetrievalOfSecurityConfigurationWhenItIsMissing() {
        SecurityLevelConfiguration securityLevelConfiguration = securityLevelConfigurationService.getSecurityConfiguration();
        assertNotNull("SecurityLevelConfiguration was found", securityLevelConfiguration);
    }

    /**
     * Test update of {@link SecurityLevelConfiguration}
     */
    @Test
    public void testUpdateSecurityConfiguration() {
        SecurityLevelConfiguration securityLevelConfiguration = securityLevelConfigurationService.getSecurityConfiguration();
        securityLevelConfiguration.setMinimumPasswordLength(100);

        SecurityLevelConfiguration foundSecurityLevelConfiguration = securityLevelConfigurationRepository.findOne(1L);
        assertEquals("SecurityLevelConfiguration was correctly updated", 100, foundSecurityLevelConfiguration.getMinimumPasswordLength());
    }
}
