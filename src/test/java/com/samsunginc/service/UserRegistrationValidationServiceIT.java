package com.samsunginc.service;

import com.samsunginc.service.UserRegistrationValidationService;
import com.samsunginc.service.SecurityLevelConfigurationService;
import com.samsunginc.model.SecurityLevelConfiguration;
import com.samsunginc.model.User;
import com.samsunginc.service.UserRegistrationValidationService.PasswordValidationOperation;
import com.samsunginc.service.UserRegistrationValidationService.PasswordValidationOperationFactory;
import com.samsunginc.service.UserRegistrationValidationService.PasswordValidationType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static com.samsunginc.service.UserRegistrationValidationService.*;
import static com.samsunginc.service.UserRegistrationValidationService.PasswordValidationType.*;
import static com.samsunginc.service.UserRegistrationValidationService.PasswordValidationType.ALPHANUMERIC;
import static org.junit.Assert.*;

/**
 * Integration Test class for {@link UserRegistrationValidationService} methods
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
@ActiveProfiles("integration")
public class UserRegistrationValidationServiceIT {

    @Autowired
    private UserRegistrationValidationService userRegistrationValidationService;

    @Autowired
    private SecurityLevelConfigurationService securityLevelConfigurationService;

    /**
     * Test success with non existent username
     */
    @Test
    @Sql("create_fake_user.sql")
    public void testUniqueValidUsername() {
        User user = new User();
        user.setUsername("$validus3rname");
        UserRegistrationValidationService.HasFieldError fieldErrors = userRegistrationValidationService.validateUsername(user);
        assertTrue(fieldErrors.getErrors().isEmpty());
    }

    /**
     * Test failure with existent username
     */
    @Test
    @Sql("create_fake_user.sql")
    public void testExistentUsername() {
        User user = new User();
        user.setUsername("fake_user");
        UserRegistrationValidationService.HasFieldError fieldError = userRegistrationValidationService.validateUsername(user);
        assertFalse(fieldError.getErrors().isEmpty());
        assertEquals(NOT_UNIQUE, fieldError.getErrors().get(0));
    }
    /**
     * Test success with non existent email address
     */
    @Test
    @Sql("create_fake_user.sql")
    public void testUniqueValidEmail() {
        User user = new User().setEmail("valid-email@domain.com");
        UserRegistrationValidationService.HasFieldError fieldErrors = userRegistrationValidationService.validateUserEmail(user);
        assertTrue(fieldErrors.getErrors().isEmpty());
    }

    /**
     * Test failure with existent email address
     */
    @Test
    @Sql("create_fake_user.sql")
    public void testExistentEmail() {
        User user = new User().setEmail("fake@email.com");
        UserRegistrationValidationService.HasFieldError fieldError = userRegistrationValidationService.validateUserEmail(user);
        assertFalse(fieldError.getErrors().isEmpty());
        assertEquals(NOT_UNIQUE, fieldError.getErrors().get(0));
    }

    /**
     * Test minimum password length {@link PasswordValidationType}
     */
    @Test
    public void testValidPasswordMinimumLength() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setMinimumPasswordLength(5);
        securityLevelConfigurationService.updateSecurityConfiguration(securityLevelConfiguration);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(MINIMUM_LENGTH, securityLevelConfiguration);
        User user = new User();
        user.setPassword("minimum");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test invalid password minimum length {@link PasswordValidationType}
     */
    @Test
    public void testInvalidPasswordMinimumLength() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setCheckMinimumLength(true);
        securityLevelConfiguration.setMinimumPasswordLength(5);
        securityLevelConfigurationService.updateSecurityConfiguration(securityLevelConfiguration);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(MINIMUM_LENGTH, securityLevelConfiguration);
        User user = new User();
        user.setPassword("min");
        assertFalse(validationOperation.hasValidPassword(user));
        assertEquals(MINIMUM, validationOperation.getValidationMessage());
    }

    /**
     * Test password with special characters {@link PasswordValidationType} and configuration enabled
     */
    @Test
    public void testSuccessPasswordWithSpecialCharacterEnabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setIncludeSpecialCharacters(true);
        securityLevelConfigurationService.updateSecurityConfiguration(securityLevelConfiguration);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(INCLUDE_SPECIAL_CHARS, securityLevelConfiguration);
        User user = new User();
        user.setPassword("pas$word");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test invalid password with special characters {@link PasswordValidationType} and configuration enabled
     */
    @Test
    public void testInvalidPasswordWithSpecialCharacterEnabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setIncludeSpecialCharacters(true);
        securityLevelConfigurationService.updateSecurityConfiguration(securityLevelConfiguration);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(INCLUDE_SPECIAL_CHARS, securityLevelConfiguration);
        User user = new User();
        user.setPassword("password");
        assertFalse(validationOperation.hasValidPassword(user));
    }

    /**
     * Test password with special characters {@link PasswordValidationType} and configuration disabled
     */
    @Test
    public void testPasswordWithSpecialCharacterDisabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setIncludeSpecialCharacters(false);
        securityLevelConfigurationService.updateSecurityConfiguration(securityLevelConfiguration);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(INCLUDE_SPECIAL_CHARS, securityLevelConfiguration);
        User user = new User();
        user.setPassword("password");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test password with alphanumeric {@link PasswordValidationType} and configuration enabled
     */
    @Test
    public void testSuccessPasswordWithAlphanumericEnabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setIncludeNumbersAndLetters(true);
        securityLevelConfigurationService.updateSecurityConfiguration(securityLevelConfiguration);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(ALPHANUMERIC, securityLevelConfiguration);
        User user = new User();
        user.setPassword("p4ssw0rd");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test invalid password with alphanumeric {@link PasswordValidationType} and configuration enabled
     */
    @Test
    public void testInvalidPasswordWithAlphanumericEnabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setIncludeNumbersAndLetters(true);
        securityLevelConfigurationService.updateSecurityConfiguration(securityLevelConfiguration);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(ALPHANUMERIC, securityLevelConfiguration);
        User user = new User();
        user.setPassword("password");
        assertFalse(validationOperation.hasValidPassword(user));
        user.setPassword("1234");
        assertFalse(validationOperation.hasValidPassword(user));
    }

    /**
     * Test password with alphanumeric {@link PasswordValidationType} and configuration disabled
     */
    @Test
    public void testPasswordWithAlphanumericDisabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setIncludeNumbersAndLetters(false);
        securityLevelConfigurationService.updateSecurityConfiguration(securityLevelConfiguration);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(ALPHANUMERIC, securityLevelConfiguration);
        User user = new User();
        user.setPassword("password");
        assertTrue(validationOperation.hasValidPassword(user));
        user.setPassword("1234");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test password non containing username or email {@link PasswordValidationType} and configuration enabled
     */
    @Test
    public void testPasswordNonContainingUserNamePasswordEnabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setExcludeUserNameAndEmail(true);
        securityLevelConfigurationService.updateSecurityConfiguration(securityLevelConfiguration);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(EXCLUDE_USER_AND_EMAIL, securityLevelConfiguration);
        User user = new User();
        user.setUsername("peter");
        user.setEmail("peter@domain.com");
        user.setPassword("p3terpan");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test invalid password containing username or email {@link PasswordValidationType} and configuration enabled
     */
    @Test
    public void testInvalidPasswordContainingUserNamePasswordEnabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setExcludeUserNameAndEmail(true);
        securityLevelConfigurationService.updateSecurityConfiguration(securityLevelConfiguration);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(EXCLUDE_USER_AND_EMAIL, securityLevelConfiguration);
        User user = new User();
        user.setUsername("peter");
        user.setEmail("peter@domain.com");
        user.setPassword("peterpan");
        assertFalse(validationOperation.hasValidPassword(user));
        user.setPassword("peter@domain.com2000");
        assertFalse(validationOperation.hasValidPassword(user));
    }

    /**
     * Test password non containing username or email {@link PasswordValidationType} and configuration disabled
     */
    @Test
    public void testPasswordContainingUserNamePasswordDisabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setExcludeUserNameAndEmail(false);
        securityLevelConfigurationService.updateSecurityConfiguration(securityLevelConfiguration);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(EXCLUDE_USER_AND_EMAIL, securityLevelConfiguration);
        User user = new User();
        user.setUsername("peter");
        user.setEmail("peter@domain.com");
        user.setPassword("peterpan");
        assertTrue(validationOperation.hasValidPassword(user));
        user.setPassword("peter@domain.com2000");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test password non containing repeating characters {@link PasswordValidationType} and configuration enabled
     */
    @Test
    public void testPasswordNonContainingRepeatingCharactersEnabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setExcludeRepeatingCharacters(true);
        securityLevelConfigurationService.updateSecurityConfiguration(securityLevelConfiguration);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(EXCLUDE_REPEATING_CHARS, securityLevelConfiguration);
        User user = new User();
        user.setPassword("asdf34gthj");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test invalid password non containing repeating characters {@link PasswordValidationType} and configuration enabled
     */
    @Test
    public void testInvalidPasswordWithRepeatingCharactersEnabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setExcludeRepeatingCharacters(true);
        securityLevelConfigurationService.updateSecurityConfiguration(securityLevelConfiguration);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(EXCLUDE_REPEATING_CHARS, securityLevelConfiguration);
        User user = new User();
        user.setPassword("petterpan");
        assertFalse(validationOperation.hasValidPassword(user));
    }

    /**
     * Test password non containing repeating characters {@link PasswordValidationType} and configuration disabled
     */
    @Test
    public void testPasswordWithRepeatingCharactersDisabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setExcludeRepeatingCharacters(false);
        securityLevelConfigurationService.updateSecurityConfiguration(securityLevelConfiguration);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(EXCLUDE_REPEATING_CHARS, securityLevelConfiguration);
        User user = new User();
        user.setPassword("peterpan");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test a success {@link SecurityLevelConfiguration}
     */
    @Test
    public void testPasswordAgainstConfiguredSecurityLevel() {
        SecurityLevelConfiguration configuration = new SecurityLevelConfiguration();
        configuration.setMinimumPasswordLength(10);
        configuration.setExcludeUserNameAndEmail(true);
        configuration.setIncludeNumbersAndLetters(true);
        configuration.setIncludeSpecialCharacters(true);
        configuration.setExcludeRepeatingCharacters(true);
        securityLevelConfigurationService.updateSecurityConfiguration(configuration);
        User user = new User();
        user.setUsername("peter");
        user.setEmail("peter@domain.com");
        user.setPassword("p3tria!250$z");
        UserRegistrationValidationService.HasFieldError fieldError = userRegistrationValidationService.validateUserPassword(user);
        assertTrue(fieldError.getErrors().isEmpty());
    }

    /**
     * Test failure password
     */
    @Test
    public void testInvalidPasswordAgainstConfiguredSecurityLevel() {
        SecurityLevelConfiguration configuration = new SecurityLevelConfiguration();
        configuration.setCheckMinimumLength(true);
        configuration.setMinimumPasswordLength(10);
        configuration.setExcludeUserNameAndEmail(true);
        configuration.setIncludeNumbersAndLetters(true);
        configuration.setIncludeSpecialCharacters(true);
        configuration.setExcludeRepeatingCharacters(true);
        securityLevelConfigurationService.updateSecurityConfiguration(configuration);
        User user = new User();
        user.setUsername("peter");
        user.setEmail("peter@domain.com");
        user.setPassword("peterr");
        UserRegistrationValidationService.HasFieldError fieldError = userRegistrationValidationService.validateUserPassword(user);
        assertFalse(fieldError.getErrors().isEmpty());
        assertEquals(5, fieldError.getErrors().size());
        assertTrue(fieldError.getErrors().contains(MINIMUM));
        assertTrue(fieldError.getErrors().contains(UserRegistrationValidationService.ALPHANUMERIC));
        assertTrue(fieldError.getErrors().contains(SPECIAL_CHARACTERS));
        assertTrue(fieldError.getErrors().contains(NOT_USER_NAME_OR_EMAIL));
        assertTrue(fieldError.getErrors().contains(REPEATING_CHARACTERS));
    }

}
