package com.samsunginc.service;

import com.samsunginc.service.SecurityLevelConfigurationService;
import com.samsunginc.model.SecurityLevelConfiguration;
import com.samsunginc.repository.SecurityLevelConfigurationRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Test class for {@link SecurityLevelConfigurationService} methods
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public class SecurityLevelConfigurationServiceTest {

    @Mock
    private SecurityLevelConfigurationRepository securityLevelConfigurationRepository;

    @InjectMocks
    private SecurityLevelConfigurationService securityLevelConfigurationService;

    @Before
    public void init() {
        initMocks(this);
    }

    /**
     * Test retrieval of a {@link SecurityLevelConfiguration}
     */
    @Test
    public void testRetrievalOfSecurityConfiguration() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setUid(999L);
        when(securityLevelConfigurationRepository.findOne(1L)).thenReturn(securityLevelConfiguration);
        SecurityLevelConfiguration foundSecurityLevelConfiguration = securityLevelConfigurationService.getSecurityConfiguration();
        assertEquals("Found existent SecurityLevelConfiguration", securityLevelConfiguration, foundSecurityLevelConfiguration);
    }

    /**
     * Test retrieval of {@link SecurityLevelConfiguration} even when it does not exist
     */
    @Test
    public void testRetrievalOfSecurityConfigurationWhenItIsMissing() {
        when(securityLevelConfigurationRepository.findOne(1L)).thenReturn(null);
        when(securityLevelConfigurationRepository.save((SecurityLevelConfiguration) anyObject())).thenReturn(new SecurityLevelConfiguration());
        SecurityLevelConfiguration securityLevelConfiguration = securityLevelConfigurationService.getSecurityConfiguration();
        assertNotNull("SecurityLevelConfiguration was found", securityLevelConfiguration);
    }

    /**
     * Test update of the {@link SecurityLevelConfiguration}
     */
    @Test
    public void testUpdateSecurityConfiguration() {
        SecurityLevelConfiguration firstSecurityLevelConfiguration = new SecurityLevelConfiguration();
        SecurityLevelConfiguration secondSecurityLevelConfiguration = new SecurityLevelConfiguration();

        when(securityLevelConfigurationRepository.findOne(1L)).thenReturn(firstSecurityLevelConfiguration);

        SecurityLevelConfiguration foundSecurityLevelConfiguration = securityLevelConfigurationService.getSecurityConfiguration();

        when(securityLevelConfigurationRepository.findOne(1L)).thenReturn(secondSecurityLevelConfiguration);

        foundSecurityLevelConfiguration.setMinimumPasswordLength(100);

        securityLevelConfigurationService.updateSecurityConfiguration(foundSecurityLevelConfiguration);

        assertEquals("SecurityLevelConfiguration was correctly modified", secondSecurityLevelConfiguration.getMinimumPasswordLength(), foundSecurityLevelConfiguration.getMinimumPasswordLength());
    }
}
