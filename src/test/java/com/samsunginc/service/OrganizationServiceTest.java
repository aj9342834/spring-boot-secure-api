package com.samsunginc.service;

import com.samsunginc.service.OrganizationService;
import com.samsunginc.model.Organization;
import com.samsunginc.repository.OrganizationRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Test class for {@link OrganizationService} methods
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public class OrganizationServiceTest {

    @Mock
    private OrganizationRepository organizationRepository;

    @InjectMocks
    private OrganizationService organizationService;

    @Before
    public void init(){
        initMocks(this);
    }


    /**
     * Test retrieval of a {@link List} of {@link Organization} objects
     */
    @Test
    public void testRetrievalOfAllOrganizations() {
        when(organizationRepository.findAll()).thenReturn(buildMockList(4));
        List<Organization> organizations = organizationService.list();
        assertEquals(4, organizations.size());
        assertEquals(Long.valueOf(2), organizations.get(2).getUid());
    }

    private List<Organization> buildMockList(int amount) {
        List<Organization> organizations = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            Organization o = new Organization();
            o.setUid(Long.valueOf(i));
            organizations.add(o);
        }
        return organizations;
    }

    /**
     * Test the retrieval of an {@link com.samsunginc.model.Organization} object by its {@code uid}
     */
    @Test
    public void testFindOneOrganization() {
        Organization organization = new Organization();
        organization.setName("Organization");
        when(organizationRepository.findOne(1L)).thenReturn(organization);
        Organization foundOrganization = organizationService.findOne(1L);
        assertEquals("Organization", foundOrganization.getName());
    }

}