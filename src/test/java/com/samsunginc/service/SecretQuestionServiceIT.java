package com.samsunginc.service;

import com.samsunginc.service.SecretQuestionService;
import com.samsunginc.model.SecretQuestion;
import com.samsunginc.repository.SecretQuestionRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Integration test class for {@link SecretQuestionService} methods
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
@ActiveProfiles("integration")
public class SecretQuestionServiceIT {

    @Autowired
    private SecretQuestionService secretQuestionService;

    /**
     * Test retrieval of list of {@link SecretQuestion} objects
     */
    @Test
    public void testRetrievalOfSecretQuestionsListIsNotEmpty() {
        List<SecretQuestion> secretQuestions = secretQuestionService.list();
        assertFalse(secretQuestions.isEmpty());
    }

    /**
     * Test retrieval of a single {@link SecretQuestion}
     */
    @Test
    @Sql("insert_secret_questions.sql")
    public void testGetOfSecretQuestionByUid() {
        SecretQuestion secretQuestion = secretQuestionService.findOne(999L);
        assertNotNull(secretQuestion);
        assertEquals("secret question", secretQuestion.getQuestion());
    }
}
