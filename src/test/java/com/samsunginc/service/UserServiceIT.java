package com.samsunginc.service;

import com.samsunginc.service.UserService;
import com.samsunginc.model.User;
import com.samsunginc.model.UserStatus;
import com.samsunginc.repository.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.subethamail.wiser.Wiser;

import static com.samsunginc.service.WiserAssertions.assertReceivedMessage;
import static org.junit.Assert.*;

/**
 * Test class for {@link User} status change related operations.
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
@ActiveProfiles("integration")
public class UserServiceIT {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @Value("${samsung.mail.defaultsender.address}")
    private String defaultSystemSender;

    @Value("${samsung.mail.admin.email}")
    private String defaultAdministratorMail;

    Wiser wiser;

    @Before
    public void setup() {
        wiser = new Wiser();
        wiser.start();
    }

    @After
    public void tearDown() {
        wiser.stop();
    }

    /**
     * Test the correct update of an {@link User}'s property after activating it.
     */
    @Test
    @Sql("create_john_doe_user.sql")
    public void testActivateUser() {
        userService.updateUserStatus(999L, UserStatus.ACTIVE);
        User user = userRepository.findOne(999L);
        assertEquals("The user was activated", UserStatus.ACTIVE, user.getUserStatus());
        assertTrue("The user was activated", user.isActive());
    }

    /**
     * Test that a mail is successfully sent after the activation of an {@link User}
     */
    @Test
    @Sql("create_john_doe_user.sql")
    public void testMailContentAfterAnUserIsActivated() {
        userService.updateUserStatus(999L, UserStatus.ACTIVE);
        User user = userRepository.findOne(999L);
        assertReceivedMessage(wiser)
                .from(defaultSystemSender)
                .to(user.getEmail())
                .withSubject("User Account Activated")
                .withMimeContentContaining("Your Samsung Agent user account has been successfully activated")
                .to(defaultAdministratorMail)
                .withSubject("User Account Notification sent")
                .withMimeContentContaining("Activated User Account Notification sent to John Doe");
    }

    /**
     * Test the correct update of an {@link User}'s property after deactivating it.
     */
    @Test
    @Sql("create_john_doe_user.sql")
    public void testDeactivateUser() {
        userService.updateUserStatus(999L, UserStatus.INACTIVE);
        User user = userRepository.findOne(999L);
        assertEquals("The user was deactivated", UserStatus.INACTIVE, user.getUserStatus());
        assertFalse("The user was deactivated", user.isActive());
    }

    /**
     * Test the deletion of an {@link User}'s property after activating it.
     */
    @Test
    @Sql("create_john_doe_user.sql")
    public void testRejectUser() {
        userService.updateUserStatus(999L, UserStatus.REJECTED);
        User user = userRepository.findOne(999L);
        assertNull("The user was removed after deactivation", user);
    }

    /**
     * Test that a mail is successfully sent after rejecting an {@link User}
     */
    @Test
    @Sql("create_john_doe_user.sql")
    public void testMailContentAfterRejectingAnUser() throws Exception {
        User user = userRepository.getOne(999L);
        userService.updateUserStatus(999L, UserStatus.REJECTED);
        assertReceivedMessage(wiser)
                .from(defaultSystemSender)
                .to(user.getEmail())
                .withSubject("User Account Request Rejected")
                .withMimeContentContaining("Dear John Doe")
                .withMimeContentContaining("Your Samsung Agent user account request has been rejected for the following reason - Access Denied");
    }

    /**
     * Test that an user status is toggled if user status is AWAITING APPROVAL
     */
    @Test
    @Sql("create_john_doe_user.sql")
    public void testCorrectToggleUserStatusFromAwaitingApproval() throws Exception {
        User user = userRepository.getOne(999L);
        userService.toggleUserStatus(user.getUid());
        user = userRepository.findOne(999L);
        assertEquals("User status set to ACTIVE", UserStatus.ACTIVE, user.getUserStatus());
    }

    /**
     * Test that an user status is being correctly toggled
     */
    @Test
    @Sql("create_active_john_doe_user.sql")
    public void testCorrectToggleUserStatus() throws Exception {
        User user = userRepository.getOne(999L);
        userService.toggleUserStatus(user.getUid());
        user = userRepository.findOne(999L);
        assertEquals("User status set to INACTIVE", UserStatus.INACTIVE, user.getUserStatus());
    }

}
