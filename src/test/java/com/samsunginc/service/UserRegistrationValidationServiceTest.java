package com.samsunginc.service;

import com.samsunginc.service.SecurityLevelConfigurationService;
import com.samsunginc.model.SecurityLevelConfiguration;
import com.samsunginc.model.User;
import com.samsunginc.repository.UserRepository;
import com.samsunginc.service.UserRegistrationValidationService.PasswordValidationOperation;
import com.samsunginc.service.UserRegistrationValidationService.PasswordValidationOperationFactory;
import com.samsunginc.service.UserRegistrationValidationService.PasswordValidationType;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static com.samsunginc.service.UserRegistrationValidationService.*;
import static com.samsunginc.service.UserRegistrationValidationService.PasswordValidationType.*;
import static com.samsunginc.service.UserRegistrationValidationService.PasswordValidationType.ALPHANUMERIC;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Test class for {@link UserRegistrationValidationService} methods
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public class UserRegistrationValidationServiceTest {

    @Mock
    private SecurityLevelConfigurationService securityLevelConfigurationService;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserRegistrationValidationService userRegistrationValidationService;

    @Before
    public void init() {
        initMocks(this);
    }

    /**
     * Test a valid username with {@link UserRegistrationValidationService#isValidUsername(String)} method
     */
    @Test
    public void testValidUsername() {
        assertTrue(userRegistrationValidationService.isValidUsername("$validus3rname"));
    }

    /**
     * Test an invalid username with {@link UserRegistrationValidationService#isValidUsername(String)} method
     */
    @Test
    public void testInvalidUsername() {
        assertFalse(userRegistrationValidationService.isValidUsername("a not valid username"));
    }

    /**
     * Test valid first name
     */
    @Test
    public void testValidFirstName() {
        User user = new User();
        user.setFirstName("lessThan60Characters");
        HasFieldError firstNameError = userRegistrationValidationService.validateFirstName(user);
        assertTrue(firstNameError.getErrors().isEmpty());
    }

    /**
     * Test invalid empty first name
     */
    @Test
    public void testInvalidEmptyFirstName() {
        User user = new User();
        user.setFirstName("");
        HasFieldError firstNameError = userRegistrationValidationService.validateFirstName(user);
        assertFalse(firstNameError.getErrors().isEmpty());
    }

    /**
     * Test to big empty first name
     */
    @Test
    public void testInvalidToBigFirstName() {
        User user = new User();
        user.setFirstName("a name with more than 60 characters is entered for the sake of the test");
        HasFieldError firstNameError = userRegistrationValidationService.validateFirstName(user);
        assertFalse(firstNameError.getErrors().isEmpty());
    }

    /**
     * Test valid last name
     */
    @Test
    public void testValidLastName() {
        User user = new User();
        user.setLastName("lessThan60Characters");
        HasFieldError firstNameError = userRegistrationValidationService.validateLastName(user);
        assertTrue(firstNameError.getErrors().isEmpty());
    }

    /**
     * Test invalid empty last name
     */
    @Test
    public void testInvalidEmptyLastName() {
        User user = new User();
        user.setLastName("");
        HasFieldError firstNameError = userRegistrationValidationService.validateLastName(user);
        assertFalse(firstNameError.getErrors().isEmpty());
    }

    /**
     * Test to big empty last name
     */
    @Test
    public void testInvalidToBigLastName() {
        User user = new User();
        user.setLastName("a name with more than 60 characters is entered for the sake of the test");
        HasFieldError firstNameError = userRegistrationValidationService.validateLastName(user);
        assertFalse(firstNameError.getErrors().isEmpty());
    }

    /**
     * Test success with non existent username
     */
    @Test
    public void testUniqueValidUsername() {
        User user = new User();
        user.setUsername("$validus3rname");
        when(userRepository.countByUsername(user.getUsername())).thenReturn(0);
        SecurityLevelConfiguration configuration = new SecurityLevelConfiguration();
        configuration.setCheckMinimumLength(true);
        configuration.setMinimumPasswordLength(10);
        configuration.setExcludeUserNameAndEmail(true);
        configuration.setIncludeNumbersAndLetters(true);
        configuration.setIncludeSpecialCharacters(true);
        configuration.setExcludeRepeatingCharacters(true);
        when(securityLevelConfigurationService.getSecurityConfiguration()).thenReturn(configuration);
        UserRegistrationValidationService.HasFieldError fieldErrors = userRegistrationValidationService.validateUsername(user);
        assertTrue(fieldErrors.getErrors().isEmpty());
    }

    /**
     * Test failure with existent username
     */
    @Test
    public void testExistentUsername() {
        User user = new User();
        user.setUsername("existent_username");
        when(userRepository.countByUsername(user.getUsername())).thenReturn(1);
        SecurityLevelConfiguration configuration = new SecurityLevelConfiguration();
        configuration.setCheckMinimumLength(true);
        configuration.setMinimumPasswordLength(10);
        configuration.setExcludeUserNameAndEmail(true);
        configuration.setIncludeNumbersAndLetters(true);
        configuration.setIncludeSpecialCharacters(true);
        configuration.setExcludeRepeatingCharacters(true);
        when(securityLevelConfigurationService.getSecurityConfiguration()).thenReturn(configuration);
        UserRegistrationValidationService.HasFieldError fieldError = userRegistrationValidationService.validateUsername(user);
        assertFalse(fieldError.getErrors().isEmpty());
        assertEquals(NOT_UNIQUE, fieldError.getErrors().get(0));
    }

    /**
     * Test a valid email with {@link UserRegistrationValidationService#isValidEmail(String)} method
     */
    @Test
    public void testValidEmail() {
        assertTrue(userRegistrationValidationService.isValidEmail("$valid-email@domain.com"));
    }

    /**
     * Test an invalid email with {@link UserRegistrationValidationService#isValidEmail(String)} method
     */
    @Test
    public void testInvalidEmail() {
        assertFalse(userRegistrationValidationService.isValidEmail("not valid@email address"));
    }

    /**
     * Test success with non existent email address
     */
    @Test
    public void testUniqueValidEmail() {
        User user = new User().setEmail("valid-email@domain.com");
        when(userRepository.countByEmail(user.getEmail())).thenReturn(0);
        UserRegistrationValidationService.HasFieldError fieldErrors = userRegistrationValidationService.validateUserEmail(user);
        assertTrue(fieldErrors.getErrors().isEmpty());
    }

    /**
     * Test failure with existent email address
     */
    @Test
    public void testExistentEmail() {
        User user = new User().setEmail("existent-email@domain.com");
        when(userRepository.countByEmail(user.getEmail())).thenReturn(1);
        UserRegistrationValidationService.HasFieldError fieldError = userRegistrationValidationService.validateUserEmail(user);
        assertFalse(fieldError.getErrors().isEmpty());
        assertEquals(NOT_UNIQUE, fieldError.getErrors().get(0));
    }

    /**
     * Test minimum password length {@link PasswordValidationType}
     */
    @Test
    public void testValidPasswordMinimumLength() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setCheckMinimumLength(true);
        securityLevelConfiguration.setMinimumPasswordLength(5);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(MINIMUM_LENGTH, securityLevelConfiguration);
        User user = new User();
        user.setPassword("minimum");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test invalid password minimum length {@link PasswordValidationType}
     */
    @Test
    public void testInvalidPasswordMinimumLength() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setCheckMinimumLength(true);
        securityLevelConfiguration.setMinimumPasswordLength(5);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(MINIMUM_LENGTH, securityLevelConfiguration);
        User user = new User();
        user.setPassword("min");
        assertFalse(validationOperation.hasValidPassword(user));
        assertEquals(MINIMUM, validationOperation.getValidationMessage());
    }

    /**
     * Test password disabling minimum length check with {@link PasswordValidationType}
     */
    @Test
    public void testPasswordWithDisabledLengthCheck() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setCheckMinimumLength(false);
        securityLevelConfiguration.setMinimumPasswordLength(10);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(MINIMUM_LENGTH, securityLevelConfiguration);
        User user = new User();
        user.setPassword("min");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test password with special characters {@link PasswordValidationType} and configuration enabled
     */
    @Test
    public void testSuccessPasswordWithSpecialCharacterEnabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setIncludeSpecialCharacters(true);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(INCLUDE_SPECIAL_CHARS, securityLevelConfiguration);
        User user = new User();
        user.setPassword("pas$word");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test invalid password with special characters {@link PasswordValidationType} and configuration enabled
     */
    @Test
    public void testInvalidPasswordWithSpecialCharacterEnabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setIncludeSpecialCharacters(true);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(INCLUDE_SPECIAL_CHARS, securityLevelConfiguration);
        User user = new User();
        user.setPassword("password");
        assertFalse(validationOperation.hasValidPassword(user));
    }

    /**
     * Test password with special characters {@link PasswordValidationType} and configuration disabled
     */
    @Test
    public void testPasswordWithSpecialCharacterDisabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setIncludeSpecialCharacters(false);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(INCLUDE_SPECIAL_CHARS, securityLevelConfiguration);
        User user = new User();
        user.setPassword("password");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test password with alphanumeric {@link PasswordValidationType} and configuration enabled
     */
    @Test
    public void testSuccessPasswordWithAlphanumericEnabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setIncludeNumbersAndLetters(true);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(ALPHANUMERIC, securityLevelConfiguration);
        User user = new User();
        user.setPassword("p4ssw0rd");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test invalid password with alphanumeric {@link PasswordValidationType} and configuration enabled
     */
    @Test
    public void testInvalidPasswordWithAlphanumericEnabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setIncludeNumbersAndLetters(true);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(ALPHANUMERIC, securityLevelConfiguration);
        User user = new User();
        user.setPassword("password");
        assertFalse(validationOperation.hasValidPassword(user));
        user.setPassword("1234");
        assertFalse(validationOperation.hasValidPassword(user));
    }

    /**
     * Test password with alphanumeric {@link PasswordValidationType} and configuration disabled
     */
    @Test
    public void testPasswordWithAlphanumericDisabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setIncludeNumbersAndLetters(false);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(ALPHANUMERIC, securityLevelConfiguration);
        User user = new User();
        user.setPassword("password");
        assertTrue(validationOperation.hasValidPassword(user));
        user.setPassword("1234");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test password non containing username or email {@link PasswordValidationType} and configuration enabled
     */
    @Test
    public void testPasswordNonContainingUserNamePasswordEnabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setExcludeUserNameAndEmail(true);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(EXCLUDE_USER_AND_EMAIL, securityLevelConfiguration);
        User user = new User();
        user.setUsername("peter");
        user.setEmail("peter@domain.com");
        user.setPassword("p3terpan");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test invalid password containing username or email {@link PasswordValidationType} and configuration enabled
     */
    @Test
    public void testInvalidPasswordContainingUserNamePasswordEnabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setExcludeUserNameAndEmail(true);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(EXCLUDE_USER_AND_EMAIL, securityLevelConfiguration);
        User user = new User();
        user.setUsername("peter");
        user.setEmail("peter@domain.com");
        user.setPassword("peterpan");
        assertFalse(validationOperation.hasValidPassword(user));
        user.setPassword("peter@domain.com2000");
        assertFalse(validationOperation.hasValidPassword(user));
    }

    /**
     * Test password non containing username or email {@link PasswordValidationType} and configuration disabled
     */
    @Test
    public void testPasswordContainingUserNamePasswordDisabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setExcludeUserNameAndEmail(false);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(EXCLUDE_USER_AND_EMAIL, securityLevelConfiguration);
        User user = new User();
        user.setUsername("peter");
        user.setEmail("peter@domain.com");
        user.setPassword("peterpan");
        assertTrue(validationOperation.hasValidPassword(user));
        user.setPassword("peter@domain.com2000");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test password non containing repeating characters {@link PasswordValidationType} and configuration enabled
     */
    @Test
    public void testPasswordNonContainingRepeatingCharactersEnabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setExcludeRepeatingCharacters(true);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(EXCLUDE_REPEATING_CHARS, securityLevelConfiguration);
        User user = new User();
        user.setPassword("asdf34gthj");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test invalid password non containing repeating characters {@link PasswordValidationType} and configuration enabled
     */
    @Test
    public void testInvalidPasswordWithRepeatingCharactersEnabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setExcludeRepeatingCharacters(true);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(EXCLUDE_REPEATING_CHARS, securityLevelConfiguration);
        User user = new User();
        user.setPassword("petterpan");
        assertFalse(validationOperation.hasValidPassword(user));
    }

    /**
     * Test password non containing repeating characters {@link PasswordValidationType} and configuration disabled
     */
    @Test
    public void testPasswordWithRepeatingCharactersDisabled() {
        SecurityLevelConfiguration securityLevelConfiguration = new SecurityLevelConfiguration();
        securityLevelConfiguration.setExcludeRepeatingCharacters(false);
        PasswordValidationOperation validationOperation =
                PasswordValidationOperationFactory.buildValidationOperation(EXCLUDE_REPEATING_CHARS, securityLevelConfiguration);
        User user = new User();
        user.setPassword("peterpan");
        assertTrue(validationOperation.hasValidPassword(user));
    }

    /**
     * Test a success {@link SecurityLevelConfiguration}
     */
    @Test
    public void testPasswordAgainstConfiguredSecurityLevel() {
        SecurityLevelConfiguration configuration = new SecurityLevelConfiguration();
        configuration.setCheckMinimumLength(true);
        configuration.setMinimumPasswordLength(10);
        configuration.setExcludeUserNameAndEmail(true);
        configuration.setIncludeNumbersAndLetters(true);
        configuration.setIncludeSpecialCharacters(true);
        configuration.setExcludeRepeatingCharacters(true);
        when(securityLevelConfigurationService.getSecurityConfiguration()).thenReturn(configuration);
        User user = new User();
        user.setUsername("peter");
        user.setEmail("peter@domain.com");
        user.setPassword("p3tria!250$z");
        UserRegistrationValidationService.HasFieldError fieldError = userRegistrationValidationService.validateUserPassword(user);
        assertTrue(fieldError.getErrors().isEmpty());
    }

    /**
     * Test failure password
     */
    @Test
    public void testInvalidPasswordAgainstConfiguredSecurityLevel() {
        SecurityLevelConfiguration configuration = new SecurityLevelConfiguration();
        configuration.setCheckMinimumLength(true);
        configuration.setMinimumPasswordLength(10);
        configuration.setExcludeUserNameAndEmail(true);
        configuration.setIncludeNumbersAndLetters(true);
        configuration.setIncludeSpecialCharacters(true);
        // TODO: set configuration to true
        configuration.setExcludeRepeatingCharacters(false);
        when(securityLevelConfigurationService.getSecurityConfiguration()).thenReturn(configuration);
        User user = new User();
        user.setUsername("peter");
        user.setEmail("peter@domain.com");
        user.setPassword("peter");
        UserRegistrationValidationService.HasFieldError fieldError = userRegistrationValidationService.validateUserPassword(user);
        assertFalse(fieldError.getErrors().isEmpty());
        assertEquals(4, fieldError.getErrors().size());
        assertTrue(fieldError.getErrors().contains(MINIMUM));
        assertTrue(fieldError.getErrors().contains(UserRegistrationValidationService.ALPHANUMERIC));
        assertTrue(fieldError.getErrors().contains(SPECIAL_CHARACTERS));
        assertTrue(fieldError.getErrors().contains(NOT_USER_NAME_OR_EMAIL));
    }

    /**
     * Test password with configurations disabled
     */
    @Test
    public void testPasswordSecurityLevelDisabled() {
        SecurityLevelConfiguration configuration = new SecurityLevelConfiguration();
        configuration.setCheckMinimumLength(false);
        configuration.setMinimumPasswordLength(10);
        configuration.setExcludeUserNameAndEmail(false);
        configuration.setIncludeNumbersAndLetters(false);
        configuration.setIncludeSpecialCharacters(false);
        configuration.setExcludeRepeatingCharacters(false);
        when(securityLevelConfigurationService.getSecurityConfiguration()).thenReturn(configuration);
        User user = new User();
        user.setUsername("peter");
        user.setEmail("peter@domain.com");
        user.setPassword("peter");
        UserRegistrationValidationService.HasFieldError fieldError = userRegistrationValidationService.validateUserPassword(user);
        assertTrue(fieldError.getErrors().isEmpty());
        assertFalse(fieldError.getErrors().contains("The password should contain at least 10 characters"));
        assertFalse(fieldError.getErrors().contains("The password should contain letters and numbers"));
        assertFalse(fieldError.getErrors().contains("The password should contain a special characters"));
        assertFalse(fieldError.getErrors().contains("The password should not contain the username or email address"));

    }
}
