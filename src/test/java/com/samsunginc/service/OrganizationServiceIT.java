package com.samsunginc.service;

import com.samsunginc.service.OrganizationService;
import com.samsunginc.model.Organization;
import com.samsunginc.repository.OrganizationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Integration test class for {@link OrganizationService}
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
@ActiveProfiles("integration")
public class OrganizationServiceIT {

    @Autowired
    private OrganizationService organizationService;

    @Before
    public void init(){
        initMocks(this);
    }


    /**
     * Test retrieval of a {@link List} of {@link Organization} objects
     */
    @Test
    @Sql("insert_organization.sql")
    public void testRetrievalOfAllOrganizations() {
        List<Organization> organizations = organizationService.list();
        assertFalse(organizations.isEmpty());
    }

    /**
     * Test the retrieval of an {@link Organization} object by its {@code uid}
     */
    @Test
    @Sql("insert_organization.sql")
    public void testFindOneOrganization() {
        Organization foundOrganization = organizationService.findOne(999L);
        assertEquals("Organization", foundOrganization.getName());
    }

}