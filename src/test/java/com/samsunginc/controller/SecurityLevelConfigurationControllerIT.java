package com.samsunginc.controller;

import com.samsunginc.model.SecurityLevelConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Integration test class for {@link SecurityLevelConfigurationController} methods
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration")
public class SecurityLevelConfigurationControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setup() {
        restTemplate = restTemplate.withBasicAuth("tester", "test");
    }

    /**
     * Test response's status of get method
     */
    @Test
    public void testGetSecurityConfigurationMethodStatus() {
        ResponseEntity<SecurityLevelConfiguration> response = restTemplate.exchange("/api/v1.0/security-level-configuration", HttpMethod.GET, null, SecurityLevelConfiguration.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    /**
     * Test response's status of update method
     */
    @Test
    public void testUpdateSecurityConfigurationStatusCode() {
        ResponseEntity<SecurityLevelConfiguration> response = restTemplate.exchange("/api/v1.0/security-level-configuration", HttpMethod.GET, null, SecurityLevelConfiguration.class);
        SecurityLevelConfiguration securityLevelConfiguration = response.getBody();
        assertNotNull(securityLevelConfiguration);
        securityLevelConfiguration.setMinimumPasswordLength(20);

        HttpEntity<SecurityLevelConfiguration> request = new HttpEntity<>(securityLevelConfiguration);
        response = restTemplate.exchange("/api/v1.0/securityLevelConfiguration", HttpMethod.PUT, request, SecurityLevelConfiguration.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        SecurityLevelConfiguration updatedSecurityLevelConfiguration = response.getBody();
        assertNotNull(updatedSecurityLevelConfiguration);
        assertEquals(updatedSecurityLevelConfiguration.getMinimumPasswordLength(), 20);
    }
}
