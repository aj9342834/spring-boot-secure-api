package com.samsunginc.controller;

import com.samsunginc.model.Phone;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration")
public class PhonesControllerIT {
    
    @Autowired
    private TestRestTemplate restTemplate;
    
    @Before
    public void setUp() {
        restTemplate = restTemplate.withBasicAuth("tester", "test");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of list method, of class PhonesController.
     */
    @Test
    public void testList() throws Exception {
        System.out.println("testList");
        ResponseEntity<Phone[]> response = restTemplate.exchange("/api/v1.0/phones", HttpMethod.GET, null, Phone[].class);
        
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    /**
     * Test of add method, of class PhonesController.
     */
    @Test
    public void testAdd() {
        System.out.println("testUpdate");
        Phone phone = new Phone().setSerialNumber("B02");
        HttpEntity<Phone> request = new HttpEntity<>(phone);
        ResponseEntity<Phone> response = restTemplate.exchange("/api/v1.0/phones", HttpMethod.POST, request, Phone.class);
        
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(phone.getSerialNumber(), response.getBody().getSerialNumber());
    }
    
}
