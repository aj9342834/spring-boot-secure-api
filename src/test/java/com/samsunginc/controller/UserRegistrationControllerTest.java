package com.samsunginc.controller;

import com.samsunginc.controller.UserRegistrationController;
import com.samsunginc.model.User;
import com.samsunginc.service.UserRegistrationValidationService;
import com.samsunginc.service.UserService;
import com.samsunginc.validator.ValidationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.CapturingMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Test class for {@link UserRegistrationController}
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public class UserRegistrationControllerTest {
    @Mock
    UserRegistrationValidationService validationService;

    @Mock
    UserService userService;

    @InjectMocks
    UserRegistrationController userRegistrationController;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void init() {
        initMocks(this);
    }

    /**
     * Test success status of fields validation
     */
    @Test
    public void testSuccessStatusUserFieldValidation() {
        User user = new User();
        user.setPassword("test");
        when(validationService.validateUserPassword(user)).thenReturn(new UserRegistrationValidationService.HasFieldError() {
            @Override
            public String getField() {
                return "password";
            }

            @Override
            public List<String> getErrors() {
                return new ArrayList<>();
            }
        });
        ResponseEntity<User> response = userRegistrationController.validateUserRegistrationField(user, "password");
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    /**
     * Test exception thrown when errors are found in fields validations
     */
    @Test
    public void testErrorsFoundInUserFields() {
        thrown.expect(ValidationException.class);
        thrown.expect(new CapturingMatcher<ValidationException>() {
            @Override
            public boolean matches(Object argument) {
                ValidationException exception = (ValidationException) argument;
                assertTrue(argument instanceof ValidationException);
                assertFalse(exception.getErrors().isEmpty());
                return true;
            }
        });
        User user = new User();
        user.setPassword("test");
        when(validationService.validateUserPassword(user)).thenReturn(new UserRegistrationValidationService.HasFieldError() {
            @Override
            public String getField() {
                return "password";
            }

            @Override
            public List<String> getErrors() {
                return Arrays.asList("Error!");
            }
        });
        userRegistrationController.validateUserRegistrationField(user, "password");
    }
}
