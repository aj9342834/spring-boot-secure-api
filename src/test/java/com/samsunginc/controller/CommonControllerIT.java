package com.samsunginc.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration")
public class CommonControllerIT {
    
    @Autowired
    private TestRestTemplate restTemplate;
    
    @Before
    public void setUp() {
        restTemplate = restTemplate.withBasicAuth("tester", "test");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test to see if JsonDeserializer annotations are working properly
     */
    @Test
    public void testAddWithReference() {

    }
    
}
