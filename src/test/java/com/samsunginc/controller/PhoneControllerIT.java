package com.samsunginc.controller;

import com.samsunginc.model.Phone;
import java.net.URISyntaxException;
import java.util.Collections;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @see https://spring.io/blog/2016/04/15/testing-improvements-in-spring-boot-1-4
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration")
public class PhoneControllerIT {
    
    @Autowired
    private TestRestTemplate restTemplate;
    
    @Before
    public void setUp() {
        restTemplate = restTemplate.withBasicAuth("tester", "test");
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of get method, of class PhoneController.
     */
    @Test
    public void testGet() {
        System.out.println("testGet");
        ResponseEntity<Phone> response = restTemplate.exchange("/api/v1.0/phone/1", HttpMethod.GET, null, Phone.class);
        
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    /**
     * Test of get method, of class PhoneController.
     */
    @Test
    public void testGetNotFound() {
        System.out.println("testGetNotFound");
        ResponseEntity<Phone> response = restTemplate.exchange("/api/v1.0/phone/9999", HttpMethod.GET, null, Phone.class);
        
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    /**
     * Test of update method, of class PhoneController.
     */
    @Test
    public void testUpdate() throws URISyntaxException {
        System.out.println("testUpdate");
        Phone phone1 = new Phone().setSerialNumber("CON2");
        HttpEntity<Phone> request1 = new HttpEntity<>(phone1);
        ResponseEntity<Phone> response1 = restTemplate.exchange("/api/v1.0/phones", HttpMethod.POST, request1, Phone.class);
        assertEquals(HttpStatus.OK, response1.getStatusCode());
        
        Phone phone = response1.getBody().setSerialNumber("CON3");
        HttpEntity<Phone> request = new HttpEntity<>(phone);
        ResponseEntity<Phone> response = restTemplate.exchange("/api/v1.0/phone/{id}", HttpMethod.PUT, request, Phone.class, Collections.singletonMap("id", response1.getBody().getUid()));
        
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(phone.getSerialNumber(), response.getBody().getSerialNumber());
    }

    /**
     * Test of update method, of class PhoneController.
     */
    @Test
    public void testUpdateNotFound() throws URISyntaxException {
        System.out.println("testUpdateNotFound");
        Phone phone = new Phone().setSerialNumber("CON3");
        HttpEntity<Phone> request = new HttpEntity<>(phone);
        ResponseEntity<Phone> response = restTemplate.exchange("/api/v1.0/phone/{id}", HttpMethod.PUT, request, Phone.class, Collections.singletonMap("id", 999L));
        
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    /**
     * Test of delete method, of class PhoneController.
     */
    @Test
    public void testDelete() throws URISyntaxException {
        System.out.println("testDelete");
        Phone phone = new Phone().setSerialNumber("CON0215");
        HttpEntity<Phone> request = new HttpEntity<>(phone);
        ResponseEntity<Phone> response1 = restTemplate.exchange("/api/v1.0/phones", 
                HttpMethod.POST, request, Phone.class);
        
        ResponseEntity<Void> response2 = restTemplate.exchange("/api/v1.0/phone/{id}", 
                HttpMethod.DELETE, null, Void.class, Collections.singletonMap("id", response1.getBody().getUid()));
        
        assertEquals(HttpStatus.OK, response2.getStatusCode());
    }

    /**
     * Test of delete method, of class PhoneController.
     */
    @Test
    public void testDeleteNotFound() throws URISyntaxException {
        System.out.println("testDeleteNotFound");
        ResponseEntity<Void> response = restTemplate.exchange("/api/v1.0/phone/{id}", HttpMethod.DELETE, null, Void.class, Collections.singletonMap("id", 999L));
        
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
    
}
