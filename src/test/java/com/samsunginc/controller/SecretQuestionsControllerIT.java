package com.samsunginc.controller;

import com.samsunginc.model.SecretQuestion;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Integration Test class for {@link SecretQuestionsController} methods
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration")
public class SecretQuestionsControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setUp() {
        restTemplate = restTemplate.withBasicAuth("tester", "test");
    }

    /**
     * Test the retrieval of a {@link List} of {@link SecretQuestion}
     */
    @Test
    @Transactional
    @Sql("../service/insert_secret_questions.sql")
    public void testListOfSecretQuestionRetrieval() {
        ResponseEntity<SecretQuestion[]> response = restTemplate.exchange("/api/v1.0/secret-questions", HttpMethod.GET, null, SecretQuestion[].class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(response.getBody().length > 0);
    }
}