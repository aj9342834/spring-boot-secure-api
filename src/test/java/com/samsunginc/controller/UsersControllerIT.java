package com.samsunginc.controller;

import com.samsunginc.model.User;
import com.samsunginc.model.UserStatus;
import com.samsunginc.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration")
public class UsersControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private UserRepository userRepository;

    @Before
    public void setup() {
        restTemplate = restTemplate.withBasicAuth("tester", "test");
    }

    /**
     * Test success status of toggleUserStatus rest call
     */
    @Test
    public void testSuccessStatusOfToggleUserStatusMethod() {
        Set<Long> set = new HashSet<>();
        HttpEntity<Set<Long>> request = new HttpEntity<>(set);
        ResponseEntity<Void> exchange = restTemplate.exchange("/api/v1.0/users/userStatus/toggle", HttpMethod.POST, request, Void.class);
        assertEquals(HttpStatus.OK, exchange.getStatusCode());
    }

    /**
     * Test success status of toggleUserStatus for one User
     */
    @Test
    @Sql("../service/create_active_john_doe_user.sql")
    public void testToggleStatusForOneUser() {
        Set<Long> set = new HashSet<>();
        set.add(999L);
        HttpEntity<Set<Long>> request = new HttpEntity<>(set);
        ResponseEntity<Void> exchange = restTemplate.exchange("/api/v1.0/users/userStatus/toggle", HttpMethod.POST, request, Void.class);
        assertEquals(HttpStatus.OK, exchange.getStatusCode());
        User user = userRepository.findOne(999L);
        assertEquals("User status set to INACTIVE", UserStatus.INACTIVE, user.getUserStatus());
    }
}
