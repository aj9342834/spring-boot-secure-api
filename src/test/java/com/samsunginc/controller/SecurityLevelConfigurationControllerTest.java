package com.samsunginc.controller;

import com.samsunginc.controller.SecurityLevelConfigurationController;
import com.samsunginc.model.SecurityLevelConfiguration;
import com.samsunginc.service.SecurityLevelConfigurationService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

/**
 * Test class for {@link SecurityLevelConfigurationController}
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public class SecurityLevelConfigurationControllerTest {

    @Mock
    SecurityLevelConfigurationService securityLevelConfigurationService;

    @InjectMocks
    SecurityLevelConfigurationController securityLevelConfigurationController;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test response's status of get method
     */
    @Test
    public void testGetSecurityConfigurationMethodStatus() {
        when(securityLevelConfigurationService.getSecurityConfiguration()).thenReturn(new SecurityLevelConfiguration());
        ResponseEntity<SecurityLevelConfiguration> response = securityLevelConfigurationController.getSecurityConfiguration();
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    /**
     * Test response's status of update method
     */
    @Test
    public void testUpdateSecurityConfigurationStatusCode() {
        when(securityLevelConfigurationService.updateSecurityConfiguration((SecurityLevelConfiguration) anyObject())).thenReturn(new SecurityLevelConfiguration());
        ResponseEntity<SecurityLevelConfiguration> response = securityLevelConfigurationController.updateSecurityConfiguration(new SecurityLevelConfiguration());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}
