package com.samsunginc.controller;

import com.samsunginc.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Integration test class for {@link UserRegistrationController}
 * @author Andrew Jackson <aj9342@gmail.com>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration")
public class UserRegistrationControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Before
    public void setup() {
        restTemplate = restTemplate.withBasicAuth("tester", "test");
    }

    /**
     * Test success status of fields validation
     */
    @Test
    public void testSuccessStatusUserFieldValidation() {
        User user = new User();
        user.setUsername("uniqueUserName");
        user.setPassword("1234567890$[abcd]");
        user.setEmail("fake_email@domain.com");

        HttpEntity<User> request = new HttpEntity<>(user);
        ResponseEntity<User> exchange = restTemplate.exchange("/api/v1.0/user-registration/validate", HttpMethod.POST, request, User.class);
        assertEquals(HttpStatus.OK, exchange.getStatusCode());
    }

    /**
     * Test failure status when errors are found in fields validations
     */
    @Test
    public void testErrorsFoundInUserFields() {
        User user = new User();
        user.setUsername("uniqueUserName");
        user.setPassword("short");
        user.setEmail("fake*email@");

        HttpEntity<User> request = new HttpEntity<>(user);
        ResponseEntity<Object> exchange = restTemplate.exchange("/api/v1.0/user-registration/validate", HttpMethod.POST, request, Object.class);
        assertEquals(HttpStatus.BAD_REQUEST, exchange.getStatusCode());
        assertTrue(exchange.getBody() instanceof Map);
        Map errors = (Map) exchange.getBody();
        assertEquals(1, errors.size());
        assertEquals(4, ((List)errors.get("errors")).size());
    }
}
