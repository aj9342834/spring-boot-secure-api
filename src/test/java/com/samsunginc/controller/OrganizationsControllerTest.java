package com.samsunginc.controller;

import com.samsunginc.controller.OrganizationsController;
import com.samsunginc.model.Organization;
import com.samsunginc.service.OrganizationService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Test class for {@link OrganizationsController} methods
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public class OrganizationsControllerTest {

    @Mock
    private OrganizationService organizationService;

    @InjectMocks
    private OrganizationsController organizationsController;

    @Before
    public void init() {
        initMocks(this);
    }

    /**
     * Test the retrieval of a {@link List} of {@link Organization}
     */
    @Test
    public void testListOfOrganizationRetrieval() {
        when(organizationService.list()).thenReturn(Arrays.asList(new Organization().setUid(0L),
                new Organization().setUid(1L),
                new Organization().setUid(2L)));
        ResponseEntity<List<Organization>> response = organizationsController.list();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(3, response.getBody().size());
    }


}