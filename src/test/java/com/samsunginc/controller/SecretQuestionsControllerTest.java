package com.samsunginc.controller;

import com.samsunginc.controller.SecretQuestionsController;
import com.samsunginc.model.SecretQuestion;
import com.samsunginc.service.SecretQuestionService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Test class for {@link SecretQuestionsController} methods
 * @author Andrew Jackson <aj9342@gmail.com>
 */
public class SecretQuestionsControllerTest {

    @Mock
    private SecretQuestionService secretQuestionService;

    @InjectMocks
    private SecretQuestionsController secretQuestionsController;

    @Before
    public void init() {
        initMocks(this);
    }

    /**
     * Test the retrieval of a {@link List} of {@link SecretQuestion}
     */
    @Test
    public void testListOfSecretQuestionRetrieval() {
        when(secretQuestionService.list()).thenReturn(Arrays.asList(new SecretQuestion().setUid(0L),
                new SecretQuestion().setUid(1L),
                new SecretQuestion().setUid(2L)));
        ResponseEntity<List<SecretQuestion>> response = secretQuestionsController.list();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(3, response.getBody().size());
    }


}